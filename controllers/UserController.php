<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 04.03.2019
 * Time: 16:47
 */

namespace app\controllers;
use app\models\Comment;
use app\models\Frends;
use Tests\Behat\Gherkin\Keywords\CucumberKeywordsTest;
use Yii;
use app\controllers\CastomController;
use app\models\Message;
use app\models\User;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use rico\yii2images\models\Image;

class UserController extends CastomController
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                /*'only' => ['logout'],*/
                'rules' => [
                    /*[
                        // неавторизованному пользователю доступны только эти действия
                        'actions' => ['login', 'registration'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['message', 'interlocutor', 'right', 'addcom', 'del-mes', 'lk', 'option', 'online', 'spisok', 'edit-mes', 'del-img', 'seve-edit-mess'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * Отоброжение сообщений
     */
    public function actionMessage($id)
    {

        $userName = User::find()->where(['id' => $id])->one();

        $this->setMeta($userName->username);

        $currentUserId = Yii::$app->user->identity->getId();
        $messagesQuery = Message::findMessages($currentUserId, $id);
        Message::messageStatus($id, $currentUserId);


        $model = new Message([
            /*'from' => $currentUserId,
            'to' => $id*/
        ]);

        if ($model->load(Yii::$app->request->post()))
        {

            $model->from = $currentUserId;
            $model->to = $id;
            $model->status = 0;
            $model->creatdate = date('Y-m-d H:i:s');
            $model->save();

            $model->images = UploadedFile::getInstances($model, 'images');
            if( $model->images )
            {
                $model->uploadGallery();
            }

            $model = new Message([
                /*'from' => $currentUserId,
                'to' => $id*/
            ]);
            $messagesQuery = Message::findMessages($currentUserId, $id);
            Message::messageStatus($id, $currentUserId);
            return $this->redirect(['user/message', 'id' =>$id]);
            /*if (Yii::$app->request->isPjax)
            {
                return $this->renderAjax('_message', compact('messagesQuery', 'id', 'model'));
            }*/
        }
        /*CastomController::printr(Message::messageStatus($id, $currentUserId));
        exit;*/

        return $this->render('_message', compact('messagesQuery', 'id'/*, 'interlocutors'*/, 'model', 'userName'));

    }

    /**
     * @return string
     * Отоброжение скем переписываються пользователи
     */
    public function actionInterlocutor(){

        $this->setMeta('Сообщения');
        $messageUser = Message::find()
            ->where(['from' => Yii::$app->user->id])
            ->orWhere(['to' => Yii::$app->user->id])
            /*->groupBy('from')*/
            ->all();

        $arrayUser = [];

        foreach ($messageUser as $user)
        {
            if ($user->from != Yii:: $app->user->id)
            {
                $arrayUser[] = $user->from;
            }
            if ($user->to != Yii:: $app->user->id)
            {
                $arrayUser[] = $user->to;
            }
        }

        $arrayUser = array_unique($arrayUser);

        /*CastomController::printr($arrayUser);
        exit;*/

        //Собеседники
        $interlocutors = User::find()->where(['id' => $arrayUser])->all();

       // CastomController::printr($interlocutors);
        /*exit;*/
        if (Yii::$app->request->isPjax)
        {
            return $this->renderAjax('interlocutor', compact( 'interlocutors'));
        }
        return $this->render('interlocutor', compact( 'interlocutors'));
    }

    /**
     * @param $id
     * @return bool|string
     * Вывод ajax сообщение кто написал
     */
    public function actionRight($id){
        $this->layout = false;
        $messages = Message::find()->where(['from' => $id, 'to' => Yii::$app->user->id])->andWhere(['status' => 0])->all();
       /* CastomController::printr($messages);
        exit;*/
        if($messages){
            foreach ($messages as $mes){
                Message::messageStatus($mes->from, $mes->to);
            }
            return $this->render('left', compact('messages'));
        }else {
            return 0;
        }
    }

    /**
     * @return string
     * сохронение сообщения
     */
    public function actionAddcom (){
        $formData = Yii::$app->request->post();

        $model = new Message();

        if ($model->load(Yii::$app->request->post())) {

            $model->from = Yii::$app->user->id;
            $model->to = $model->to;
            $model->status = 0;
            $model->creatdate = date('Y-m-d H:i:s');
            $model->save();

            $model->images = UploadedFile::getInstances($model, 'images');
            if ($model->images) {
                $model->uploadGallery();
            }
        }
        $this->layout = false;
        $model->sendNewMessage(Yii::$app->user->id,  $model->to,  $model->text);
        /*return
           CastomController::printr($model) ;*/
        return $this->render('right', compact('model'));
    }

    /**
     * @param $id
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * Удаление сообщений
     */
    public function actionDelMes($id){

        $model = Message::findOne($id);
        if($model->from  != Yii::$app->user->id){
            throw new HttpException(404, 'Доступ запрещён!');
        }
        $model->removeImages();
        $model->delete();

        return 'ok';
    }

    /**
     * @param $id
     * @return string
     * Профиль
     */
    public function actionLk($id){

        $model = User::findOne($id);
        $this->setMeta($model->username);
        return $this->render('lk', compact('model'));
    }

    /**
     * @return string|Response
     * Опции
     */
    public function actionOption(){
        $model = User::findOne(Yii::$app->user->id);
        $model->scenario = 'edit';
        $this->setMeta('Редактировать профиль');
        if ($model->load(Yii::$app->request->post())) {
            //CastomController::printr($model);
            $model->save();
            $model->image = UploadedFile::getInstance($model, 'image');
            if( $model->image )
            {
                $model->upload();

            }
            return $this->redirect(['user/lk', 'id' => Yii::$app->user->id]);
        }

        return $this->render('option', compact('model'));
    }

    /**
     * @param $id
     * @return int
     * Проверка пользователя на онлайн
     */
    public function actionOnline($id){
        $model = User::findOne($id);
        $now = date("Y-m-d H:i:s", time());
        $lastVisit = date("Y-m-d H:i:s", strtotime('+1 min', $model->updated_at));

        if($now < $lastVisit){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * @return array
     * Проверка списка сообщений
     */
    public function actionSpisok(){

        //$array = Yii::$app->request->post('array');

        $messageUser = Message::find()
            ->where(['from' => Yii::$app->user->id])
            ->orWhere(['to' => Yii::$app->user->id])
            /*->groupBy('from')*/
            ->all();

        $arrayUser = [];

        foreach ($messageUser as $user)
        {
            if ($user->from != Yii:: $app->user->id)
            {
                $arrayUser[] = $user->from;
            }
            if ($user->to != Yii:: $app->user->id)
            {
                $arrayUser[] = $user->to;
            }
        }

        $arrayUser = array_unique($arrayUser);

        /*CastomController::printr($arrayUser);
        exit;*/

        //Собеседники
        $interlocutors = User::find()->where(['id' => $arrayUser])->all();

        $array = array();
        $i = 0;
        foreach ($interlocutors as $user){
            $img = $user->getImage();
            $array[$i]['id'] = $user->id;
            $array[$i]['name'] = $user->username;
            if($img != null){
                $array[$i]['img'] = $img->getUrl('300x300');
            }else{
                $array[$i]['img'] = 'http://placehold.it/300x300';
            }
            //$array[$i]['img'] = $img->getUrl('300x300');
            $array[$i]['message'] = Message::findLastMessage($user->id, Yii::$app->user->id)->text;
            $array[$i]['count'] = Message::countMessage($user->id, Yii::$app->user->id);
            $array[$i]['status'] = CastomController::onlineUser($user->id);
            $i++;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $array;
    }

    /**
     * @param $id
     * @return array|bool
     * Редактирование фото, наполнение модели
     */
    public function  actionEditMes($id){
        $model = Message::findOne($id);
        if($model->from  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            $gal = $model->getImages();



            $array = array();

            $array['text'] = $model->text;
            if(isset($gal)){
                foreach ($gal as $img){
                    $array['gal'][$img->id] = $img->getUrl();
                }
            }

            //CastomController::printr($array);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $array;
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }


    }

    /**
     * @param $id
     * @param null $img
     * @return string
     * @throws HttpException
     * Удаление фото при редактировании модели
     */
    public function actionDelImg($id, $img = null)
    {
        $model = Message::findOne($id);
        if($model->from  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            if ( $img !== null && ($image = Image::findOne($img)) !== null) {

                $model->removeImage( $image );
            }
            if (!$im = Image::find()->where(['itemId' => $id])->all())
            {
                return 'ok';
            }

            /*Устанавливаем главную картинку если мы ее удалили*/


            foreach ($model->getImages() as $imeges)
            {
                $model->setMainImage($imeges);
                break;
            }

            return 'ok';
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }

    public function actionSeveEditMess(){

        //
        $formData = Yii::$app->request->post();


        $model = Message::findOne($formData['Message']['id']);
        //CastomController::printr($model);


        /*exit;
        $model = new Message();*/

        if ($model->load(Yii::$app->request->post())) {

            $model->from = Yii::$app->user->id;
            $model->to = $model->to;
            //$model->status = 0;
            //$model->creatdate = date('Y-m-d H:i:s');
            $model->save();

            $model->images = UploadedFile::getInstances($model, 'images');
            if ($model->images) {
                $model->uploadGallery();
            }
        }

        $gal = $model->getImages();
        $array = array();

        $array['text'] = $model->text;
        $array['name'] = $model->user->username;
        $array['date'] = $model->creatdate;
        if(isset($gal)){
            foreach ($gal as $img){
                $array['gal'][$img->id]['mini'] = $img->getUrl('100x100');
                $array['gal'][$img->id]['full'] = $img->getUrl();
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $array;
        /*$this->layout = false;
        $model->sendNewMessage(Yii::$app->user->id,  $model->to,  $model->text);*/
        /*return
           CastomController::printr($model) ;
        return $this->render('right', compact('model'));*/
    }

}