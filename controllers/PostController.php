<?php

namespace app\controllers;

use app\models\Comment;
use app\models\Ocenka;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\controllers\CastomController;
use app\models\User;
use yii\web\UploadedFile;
use yii\web\Response;
use rico\yii2images\models\Image;
use yii\web\HttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends CastomController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    /*[
                        // неавторизованному пользователю доступны только эти действия
                        'actions' => ['login', 'registration'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'comment-create', 'com', 'del-com', 'edit-comment', 'like', 'dislike', 'del-img', 'all-comment', 'hidden-comment', 'count-comment',
                        'all-text', 'hidden-text'],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->setMeta('CFW');
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
        ]);
    }

    public function actionCommentCreate($text, $id_user, $id_user_otv, $id){

        $model = new Comment();
        $model->id_post = $id;
        $model->id_user = $id_user;
        $model->id_user_otv = $id_user_otv;

        /*$text = preg_replace('/(^|\s+)@([0-9a-zA-Zа-яА-Я]+)(\b|$)/', '$1<a href="#$2">@$2</a>', $text);*/
       /* $text = preg_replace('/(^|\s+)#([0-9a-zA-Zа-яА-Я]+)(\b|$)/', '$1<a href="#$2">#$2</a>', $text);*/

        $model->text = $text;
        if($model->save()){
            $this->layout = false;
            return $this->render('comment', [
                'model' => $model,
            ]);
        }else{
            return  $model->errors;
        }
        //$model->save();

        ///return $id;
    }

    public function actionCom($id){

        $model = Comment::find()->where(['id'=>$id])->one();
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')) {
            $array = array();
            $array['text'] = $model->text;
            $array['id_user_otv'] = $model->id_user_otv;
            $array['date'] = $model->date;
            $array['id'] = $model->id;
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $array;
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }

    public function actionDelCom($id){
        $model = Comment::find()->where(['id' => $id])->one();
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            $model->delete();
            return 'ok';
        }else {
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }

    public function actionEditComment($text,$id, $user = null, $date){
        $model = Comment::find()->where(['id' => $id])->one();
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            $model->text = $text;
            $model->date = $date;
            if($user != 0){
                $model->id_user_otv = $user;
            }
            if($model->save()){
                $this->layout = false;
                return CastomController::namea($model->text, $model->id_user_otv);
            }else{
                return  $model->errors;
            }
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }

    public function actionLike($id){
       /* $model = Post::find()->where(['id' => $id])->one();
        $model->like +=1;
        $model->save();
        return $model->like;*/

       $proverka = Ocenka::find()->where(['id_post' => $id])->andWhere(['id_user' => Yii::$app->user->id])->andWhere(['like' => 1])->one();

       if(!$proverka){
           $model = new Ocenka();
           $model->id_post = $id;
           $model->id_user = Yii::$app->user->id;
           $model->like = 1;
           $model->save();
           $model1 = Post::find()->where(['id' => $id])->one();
           $model1->like +=1;
           $model1->save();
           return $model1->like;
       }
       else {
           return false;
       }
    }

    public function actionDislike($id){
        /*$model = Post::find()->where(['id' => $id])->one();
        $model->dislike +=1;
        $model->save();
        return $model->dislike;*/

        $proverka = Ocenka::find()->where(['id_post' => $id])->andWhere(['id_user' => Yii::$app->user->id])->andWhere(['dislike' => 1])->one();

        if(!$proverka){
            $model = new Ocenka();
            $model->id_post = $id;
            $model->id_user = Yii::$app->user->id;
            $model->dislike = 1;
            $model->save();
            $model1 = Post::find()->where(['id' => $id])->one();
            $model1->dislike +=1;
            $model1->save();
            return $model1->dislike;
        }
        else {
            return false;
        }
    }

    public function actionDelImg($id, $img = null)
    {
        $model = $this->findModel($id);
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            if ( $img !== null && ($image = Image::findOne($img)) !== null) {

                $model->removeImage( $image );
            }
            if (!$im = Image::find()->where(['itemId' => $id])->all())
            {
                return 'ok';
            }

            /*Устанавливаем главную картинку если мы ее удалили*/


            foreach ($model->getImages() as $imeges)
            {
                $model->setMainImage($imeges);
                break;
            }

            return 'ok';
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }


    public function actionAllComment($id){
        $models = Comment::find()->where(['id_post' => $id])->all();
        $this->layout = false;
        return $this->render('allcomment', compact('models'));
    }

    public function actionHiddenComment($id){
        $comment = $this->findModel($id);
        $models = $comment->comment;
        $this->layout = false;
        return $this->render('allcomment', compact('models'));
    }

    public function actionCountComment($id){
        $model = $this->findModel($id);
        return count($model->allcomments);
    }


    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();
        $this->setMeta('Создать пост');
        if ($model->load(Yii::$app->request->post())) {

            $model->id_user = Yii::$app->user->id;
            /*if(Yii::$app->user->can('user')){
                $model->cat = 0;
            }*/
            $model->save();
            $model->images = UploadedFile::getInstances($model, 'images');
            if( $model->images )
            {
                $model->uploadGallery();
            }
            return $this->redirect(['post/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            $this->setMeta('Редактировать пост');

            if ($model->load(Yii::$app->request->post())) {

                $model->save();

                $model->images = UploadedFile::getInstances($model, 'images');
                if( $model->images )
                {
                    $model->uploadGallery();
                }

                return $this->redirect(['post/index']);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            throw new HttpException(404, 'Доступ запрещён!');
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        if($model->id_user  == Yii::$app->user->id || Yii::$app->user->can('redactor')){
            foreach ($model->allcomments as $comment){
                if($comment->id){
                    $comment->delete();
                }
                //CastomController::printr($comment->id);
            }
            $gal = $model->getImages();
            if ($gal != null){
                foreach ($gal as $img){
                    $model->removeImage( $img );
                }
            }

            $model->delete();
        }else {
            throw new HttpException(404, 'Доступ запрещён!');
        }






        return $this->redirect(['post/index']);
    }


    public function actionAllText($id){
        $model = $this->findModel($id);
        if($model->magnet != ''){
            $magnet =  '<p><a href="'.$model->magnet .'">magnet ссылка</a></p>';
        }
        return CastomController::heh($model->text,  $model->id_user) . $magnet;
    }

    public function actionHiddenText($id){
        $model = $this->findModel($id);
        if($model->magnet != ''){
            $magnet =  '<p><a href="'.$model->magnet .'">magnet ссылка</a></p>';
        }
        return CastomController::heh(CastomController::substr_close_tags($model->text, 200), $model->id_user) . $magnet;
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
