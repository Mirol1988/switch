<?php
/**
 * Created by PhpStorm.
 * User: ilyamikhalev
 * Date: 04.03.2018
 * Time: 22:42
 */

namespace app\controllers;
use app\models\User;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Yii;
class CastomController extends Controller
{


    /**
     * Обновление даты последнего визита
     */
    public function init() {
        parent::init();
        if(!Yii::$app->user->isGuest){
            $user = User::findOne(Yii::$app->user->id);
            $user->touch('updated_at');
        }

    }

    public static function onlineUser($id){
        $model = User::findOne($id);
        $now = date("Y-m-d H:i:s", time());
        $lastVisit = date("Y-m-d H:i:s", strtotime('+1 min', $model->updated_at));

        /*if($now < $lastVisit){
            return '<span class="green">Онлайн</span>';
        }else{
            return '<span class="red">Офлайн</span>';
        } */
        if($now < $lastVisit){
            return 1;
        }else{
            return 0;
        }
    }
    /**
     * Возвращаем
     * @param null $title
     * @param null $description
     * @param null $keywords
     *
     *
     */
    protected function setMeta ($title = null, $description = null, $keywords = null)
    {
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => $keywords]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => $description]);
    }

    /**
     * Удобный вывод ошибок
     * @param $value
     */

    public static function printr($value)
    {
        echo "<pre>";
        print_r($value);
        echo "</pre>";
    }

    /**
     * Переделываем с русского на английский
     * @param $name
     * @return mixed|null|string|string[]
     */
    public static function translit($name)
    { //$name=strtolower($name);
        $name = mb_strtolower($name, 'utf-8');
        $name=trim($name);
        $name=str_replace("а", "a", $name);
        $name=str_replace("б", "b", $name);
        $name=str_replace("в", "v", $name);
        $name=str_replace("г", "g", $name);
        $name=str_replace("д", "d", $name);
        $name=str_replace("е", "e", $name);
        $name=str_replace("ё", "e", $name);
        $name=str_replace("ж", "zh", $name);
        $name=str_replace("з", "z", $name);
        $name=str_replace("и", "i", $name);
        $name=str_replace("й", "j", $name);
        $name=str_replace("к", "k", $name);
        $name=str_replace("л", "l", $name);
        $name=str_replace("м", "m", $name);
        $name=str_replace("н", "n", $name);
        $name=str_replace("о", "o", $name);
        $name=str_replace("п", "p", $name);
        $name=str_replace("р", "r", $name);
        $name=str_replace("с", "s", $name);
        $name=str_replace("т", "t", $name);
        $name=str_replace("у", "u", $name);
        $name=str_replace("ф", "f", $name);
        $name=str_replace("х", "h", $name);
        $name=str_replace("ц", "c", $name);
        $name=str_replace("ч", "ch", $name);
        $name=str_replace("ш", "sch", $name);
        $name=str_replace("щ", "sh", $name);
        $name=str_replace("ъ", "j", $name);
        $name=str_replace("ы", "y", $name);
        $name=str_replace("ь", "", $name);
        $name=str_replace("э", "e", $name);
        $name=str_replace("ю", "yu", $name);
        $name=str_replace("я", "ya", $name);
        $name=str_replace(" ", "-", $name);
        $name=str_replace("_", "-", $name);
        return $name;
    }

    /**
     * @param $string
     * @return bool|string
     * Обрезаем по кол-во символов
     */
    public function str($string, $length)
    {
        $text = htmlspecialchars($string);

        return strlen($text);

        /*$string = substr(htmlspecialchars($string), 0, $length);
        $string = rtrim($string, "!,.-");
        return $string;*/
    }

    /**
     * @param $code
     * @param int $limit
     * @return string
     * Обрезаем текст
     */
    public static function substr_close_tags($code, $limit)
    {
        if ( strlen($code) <= $limit )
        {
            return $code;
        }

        $html = substr($code, 0, $limit);
        preg_match_all ( "#<([a-zA-Z]+)#", $html, $result );

        foreach($result[1] AS $key => $value)
        {
            if ( strtolower($value) == 'br' )
            {
                unset($result[1][$key]);
            }
        }
        $openedtags = $result[1];

        preg_match_all ( "#</([a-zA-Z]+)>#iU", $html, $result );
        $closedtags = $result[1];

        foreach($closedtags AS $key => $value)
        {
            if ( ($k = array_search($value, $openedtags)) === FALSE )
            {
                continue;
            }
            else
            {
                unset($openedtags[$k]);
            }
        }

        if ( empty($openedtags) )
        {
            if ( strpos($code, ' ', $limit) == $limit )
            {
                return $html."...";
            }
            else
            {
                return substr($code, 0, strpos($code, ' ', $limit))."...";
            }
        }

        $position = 0;
        $close_tag = '';
        foreach($openedtags AS $key => $value)
        {
            $p = strpos($code, ('</'.$value.'>'), $limit);

            if ( $p === FALSE )
            {
                $code .= ('</'.$value.'>');
            }
            else if ( $p > $position )
            {
                $close_tag = '</'.$value.'>';
                $position = $p;
            }
        }

        if ( $position == 0 )
        {
            return $code;
        }

        return substr($code, 0, $position).$close_tag."...";
    }




    public static function countStr($string){
        return iconv_strlen($string);
    }


    public static function wph_cut_by_words($maxlen, $text) {
        $len = (mb_strlen($text) > $maxlen)? mb_strripos(mb_substr($text, 0, $maxlen), ' ') : $maxlen;
        $cutStr = mb_substr($text, 0, $len);
        $temp = (mb_strlen($text) > $maxlen)? $cutStr. '...' : $cutStr;
        return $temp;
    }

    /**
     * Преобразует дату из вида 20.04.1983 в вид 1983-04-20 и наоборот в зависимости от введенной даты
     *
     * @param string $date Дата для преобразования
     * @return string Инвертированная дата
     */

    public static function invertDate($date)
    {
        if(stristr($date, '-')) {
            $invertDirection = 'db2human';
            $delimeter = '-';
            $delimeterOut = '.';
        } elseif(stristr($date, '.')) {
            $invertDirection = 'human2db';
            $delimeter = '.';
            $delimeterOut = '-';
        } else {
            return $date;
        }
        // разбиваем дату на части, меняем их местами
        $dateArray = explode($delimeter, $date);

        $dateArray = array_reverse($dateArray);

        // снова склеиваем части даты в строку с новым разделителем
        return implode($delimeterOut, $dateArray);
    }

    /**
     * функция преобразует дату из формата 2012-09-01 12:30:00 в Пн 1 сен. 2012 12:30
     *  @param string $date Дата в формате Дата+время
     *  @param boolean $showTime Покузывать ли время
     *  @param boolean $showWeekday Покузывать ли день недели
     * @return string Строка в формате Пн 1 сен. 2012 12:30
     */
    public static function niceDate($date,$showTime=true,$showWeekday=true)
    {
        $monthsArray = array('','янв.', 'фев.','мар.','апр.','мая','июн.','июл.','авг.','сен.','окт.','ноя.','дек.');
        $weekDaysArray = array('Вс','Пн','Вт','Ср','Чт','Пт','Сб');
        $timestamp = strtotime($date);
        $weekdayNumber = date('w',$timestamp);
        $weekday = $weekDaysArray[$weekdayNumber];
        $dateTimeArray = self::dateTimeArray($date);

        $dateString = $dateTimeArray['day'] . " " . $monthsArray[$dateTimeArray['month']] . " " . $dateTimeArray['year'];

        if($showTime===true)
        {
            $dateString.= " ".$dateTimeArray['hours'].":".$dateTimeArray['minutes'];
        }
        if($showWeekday===true)
        {
            $dateString = $weekday . " " . $dateString;
        }
        return $dateString;
    }

    /**
     * функция принимает дату yyyy-mm-dd hh:mm:ss и возвращает массив из года, месяца, дня, часа, минуты, секунды, даты и времени
     */
    public static function dateTimeArray($dateTime)
    {
        if(stristr($dateTime," "))
        {
            //это дата+время
            $dateTimeArray=explode(" ",$dateTime);
            $dateArray = explode("-",$dateTimeArray[0]);
            $time = $dateTimeArray[1];
            $year = (int)$dateArray[0];
            $month = (int)$dateArray[1];
            $day = (int)$dateArray[2];
            $timeArray = explode(":",$time);
            $hours = (int)$timeArray[0];
            $minutes = $timeArray[1];
            $seconds = $timeArray[2];
            $outputArray = Array(
                'year'=>$year,
                'month'=>$month,
                'day'=>$day,
                'hours'=>$hours,
                'minutes'=>$minutes,
                'seconds'=>$seconds,
                'date'=>$dateTimeArray[0],
                'time'=>$dateTimeArray[1]
            );
            return $outputArray;
        } elseif(stristr($dateTime,"-")) {
            $dateArray = explode("-",$dateTime);
            $year = (int)$dateArray[0];
            $month = (int)$dateArray[1];
            $day = (int)$dateArray[2];
            $outputArray = Array(
                'year'=>$year,
                'month'=>$month,
                'day'=>$day,
            );
            return $outputArray;
        } else {
            return NULL;
        }
    }

    /**
     * Проверяем первый элемент массива на пустату
     *
     * @param $array
     * @return bool
     */

    public static function isNullArray($array)
    {

        if ($array[0] != '')
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Удаление пустых элементов в массиве
     * @param $array
     * @return mixed
     */
    public static function isDiffArray($array)
    {
        return array_diff($array, array(''));
    }


    public static function name($name){
        $name = explode('(', $name);
        $name2 = $name[0];
        $name2 = explode('[', $name2);
        return mb_strimwidth($name2[0], 0, 25, "...");
    }

    public static function Namea($text, $id){
        /*$text = preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2" target="_blank">Ссылка</a>$3',$text);*/
        $text = preg_replace('/(^|\s+)@([0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="'.Url::to(['/user/view', 'id' => $id]).'">@$2</a>', $text);
        $text = preg_replace('/(^|\s+)#([0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);
        $text = preg_replace('/(^|\s+)@([0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);
        $text = preg_replace('/(^|\s+)#([0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);

        return $text;
    }


    public static function Heh($text, $id){
       /* $text = preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2" target="_blank">Ссылка</a>$3',$text);*/
        $text = preg_replace('/(^|\s+)<p>@([-0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">@$2</a>', $text);
        $text = preg_replace('/(^|\s+)<p>#([-0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);
        $text = preg_replace('/(^|\s+)@([-0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);
        $text = preg_replace('/(^|\s+)#([-0-9a-zA-ZА-Яа-я_-]+)(\b|$)/u', '$1<a href="#">#$2</a>', $text);
        $text=preg_replace('/(<img(.*?)src="(.*?)"(.*?)>)/is', "<a href='$3' data-fancybox='gallery' class='gal'>$1</a>", $text);
        //$text=preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2" target="_blank">Ссылка</a>$3',$text);

        return $text;
    }

    function to_link($string){

        return  preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2" target="_blank">Ссылка</a>$3',$string);

    }

    /**
     * @param $id
     * @return mixed
     * Получаем роль пользователя
     */
    public static function currentUserRoleIs($id) {
        $userRole = current(ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($id), 'name'));
        return $userRole;
    }

    public static function getUserImg(){
        $model = User::findOne(Yii::$app->user->id);
        $img = $model->getImage();
        if($img != null ){
            return $img->getUrl('300x300');
        }else{
            return null;
        }

    }



    /*public static function getUser($id){
        $model = User::findOne($id);
        return $model;
    }*/

}