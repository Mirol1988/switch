<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */

$params = [
    'prompt' => 'Выберите категорию'
];
?>



<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <?= $form->field($model, 'xlsx')->fileInput(['accept' => '.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel']) ?>

        <?= $form->field($model, 'cat')->dropDownList(Yii::$app->params['gamesCat'],$params);?>

        <?= Html::submitButton('Парсить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
