<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = 'Create Games';
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-create">

    <?php $i = 1; if ($new): ?>
        <p>Добавлено новых игр</p>
        <?php foreach ($new as $n) :?>
            <p> <?=$i?>. <?=$n?></p>
        <?php $i++; endforeach; ?>

    <?php endif; ?>

    <?php $i = 1; if ($new): ?>
        <p>Отредактированно игр:</p>
        <?php foreach ($old as $o) :?>
            <p> <?=$i?>. <?=$o?></p>
        <?php $i++; endforeach; ?>

    <?php endif; ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
