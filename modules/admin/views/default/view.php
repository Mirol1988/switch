<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редоктировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'email:email',
            'username',
            [
                  'attribute' => 'active',
                    'value' => function($data){
                        $str ='';
                        if($data->active == 1){
                            $str ='Аккаунт активирован';
                        }
                        else{
                            $str ='Аккаунт не активирован';
                        }
                        return $str;
                    },
            ],
            /*'phone',*/
            'invite',
            [
                    'attribute' => 'role',
                    'value' => function($data){
                        $auth = Yii::$app->authManager;
                        $userRoles = $auth->getRolesByUser($data->id);
                        $str ='';
                        foreach ($userRoles as $roleName => $role){
                            $str  .= $role->description.'<br>';
                        }
                        return $str;
                    },
                'format' => 'html'

            ]
        ],
    ]) ?>

</div>
