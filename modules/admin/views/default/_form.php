<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$items = [
    '1' => 'Активирован или активировать',
    '0' => 'Заблокировать выдать бан',
];
$params = [
    'prompt' => 'Выберите статус...'
];


$itemInvite = [
  '0' => 'Нет Инвайта',
  '1' => 'Обычный инвайт',
  '2' => 'VIP инвайт'
];

$paramsInvite = [
    'prompt' => 'Выберите доступ'
];
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invite')->dropDownList($itemInvite,$paramsInvite);?>

    <?= $form->field($model, 'active')->dropDownList($items,$params);?>


    <?= $form->field($model, 'role')->checkboxList($allRoles) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
