<?php

namespace app\modules\admin\controllers;

use app\controllers\CustomController;
use Yii;
use app\models\User;
use app\modules\admin\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public $Password; //Хранит временый пароль

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'get'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        //упровление ролями пользователя
        $auth = Yii::$app->authManager;
        //массив ролей присутвующих в системе
        $allRolesRaw = $auth->getRoles();

        foreach ($allRolesRaw as $roleName => $role){
            $allRoles[$roleName] = $role->description;
        }


        if ($model->load(Yii::$app->request->post())) {


            $this->Password = $model->password;
            $model->password = Yii::$app->security->generatePasswordHash($model->password);
            $model->code = Yii::$app->getSecurity()->generateRandomString(10);

            /*CustomController::printr($model->role);
            exit;*/

            if($model->save()){
                /*
                * Добавление роль пользователя и сохраняем
                */


                if(is_array($model->role)){
                    foreach ($model->role as $role){
                        $roleObject = $auth->getRole($role);
                        $auth->assign($roleObject, $model->id);
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

            else{
                $model->password = $this->Password;
            }


        }

        return $this->render('create', [
            'model' => $model,
            'allRoles' => $allRoles,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //упровление ролями пользователя
        $auth = Yii::$app->authManager;
        //Массив ролей пользователя
        $userRoles = $auth->getRolesByUser($model->id);
        //массив ролей присутвующих в системе
        $allRolesRaw = $auth->getRoles();

        foreach ($allRolesRaw as $roleName => $role){
            $allRoles[$roleName] = $role->description;
        }

        foreach ($userRoles as $roleName => $role){
            $model->role[] = $roleName;
        }


        if ($model->load(Yii::$app->request->post())) {

            if($model->save()){
                $this->Password = $model->password;
                $model->password = Yii::$app->security->generatePasswordHash($model->password);

                //Удаляем права пользователя
                $auth->revokeAll($model->id);

                /*
               * Добавление роль пользователя и сохраняем
               */
                if(is_array($model->role)){
                    foreach ($model->role as $role){
                        $roleObject = $auth->getRole($role);
                        $auth->assign($roleObject, $model->id);
                    }
                }
            }
            else{
                $model->password = $this->Password;
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }




        return $this->render('update', [
            'model' => $model,
            'allRoles' => $allRoles,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
