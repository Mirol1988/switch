<?php

namespace app\modules\admin\controllers;

use app\controllers\CastomController;
use Yii;
use app\models\Games;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use alex290\spreadsheet\Excel;
/**
 * ParsController implements the CRUD actions for Games model.
 */
class ParsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Games models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Games::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Games model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Games model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Games();
        $model->scenario = 'pars';
        $old = array();
        $new = array();
        if ($model->load(Yii::$app->request->post())) {

            $model->xlsx = UploadedFile::getInstance($model, 'xlsx');
            if( $model->xlsx )
            {

               /* CastomController::printr(Yii::getAlias('@webroot'). '/'.$model->uploadFile());
                exit;*/
                $file = Yii::getAlias('@webroot'). '/'.$model->uploadFile();

                $data = Excel::import($file);



                foreach ($data as $key => $value){

                    $game = Games::find()->where(['title' => $value['Name'], 'cat' => $model->cat])->one();

                    if($game){


                        switch ($model->cat){
                            case 1 :
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                                $game->onedrive1 = $value['onedrive'];
                                $game->onedrive2 = $value['onedrivemirror2'];
                                $game->f = $value['1F'];
                                $game->gmirror1 = $value['gmirror1'];
                                $game->gmirror2 = $value['gmirror2'];
                                $game->untrimmed = $value['Untrimmed'];
                                $game->md5 = $value['MD5'];
                            break;
                            case 2:
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                                $game->onedrive1 = $value['onedrive'];
                                $game->onedrive2 = $value['onedrivemirror2_'];
                                $game->f = $value['1F'];
                                $game->gmirror1 = $value['gmirror1'];
                                $game->gmirror2 = $value['gmirror2'];
                                $game->md5 = $value['MD5'];
                            break;
                            case 3 :
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                                $game->onedrive1 = $value['onedrive'];
                                $game->onedrive2 = $value['onedrivemirror2'];
                                $game->f = $value['1F'];
                                $game->gmirror1 = $value['gmirror1'];
                                $game->gmirror2 = $value['gmirror2'];
                                $game->md5 = $value['MD5'];
                            break;
                            case 4 :
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                                $game->onedrive1 = $value['onedrive'];
                                $game->onedrive2 = $value['onedrivemirror2'];
                                $game->f = $value['1F'];
                                $game->gmirror1 = $value['gmirror1'];
                                $game->gmirror2 = $value['gmirror2'];
                                $game->md5 = $value['MD5'];
                            break;
                            case 5 :
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                                $game->onedrive1 = $value['onedrive'];
                                $game->onedrive2 = $value['onedrivemirror2'];
                                $game->f = $value['1F'];
                                $game->gmirror1 = $value['gmirror1'];
                                $game->gmirror2 = $value['gmirror2'];
                                $game->md5 = $value['MD5'];
                            break;
                            case 6 :
                                $game->title = $value['Name'];
                                $game->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $game->cat = $model->cat;
                                $game->url = $value['URL'];
                            break;

                        }
                        $game->save();
                        $old[] = $game->title;
                    }else{
                        switch ($model->cat){
                            case 1 :
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                $model->onedrive1 = $value['onedrive'];
                                $model->onedrive2 = $value['onedrivemirror2'];
                                $model->f = $value['1F'];
                                $model->gmirror1 = $value['gmirror1'];
                                $model->gmirror2 = $value['gmirror2'];
                                $model->untrimmed = $value['Untrimmed'];
                                $model->md5 = $value['MD5'];
                                break;
                            case 2:
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                $model->onedrive1 = $value['onedrive'];
                                $model->onedrive2 = $value['onedrivemirror2_'];
                                $model->f = $value['1F'];
                                $model->gmirror1 = $value['gmirror1'];
                                $model->gmirror2 = $value['gmirror2'];
                                $model->md5 = $value['MD5'];
                                break;
                            case 3 :
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                $model->onedrive1 = $value['onedrive'];
                                $model->onedrive2 = $value['onedrivemirror2'];
                                $model->f = $value['1F'];
                                $model->gmirror1 = $value['gmirror1'];
                                $model->gmirror2 = $value['gmirror2'];
                                $model->md5 = $value['MD5'];
                                break;
                            case 4 :
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                $model->onedrive1 = $value['onedrive'];
                                $model->onedrive2 = $value['onedrivemirror2'];
                                $model->f = $value['1F'];
                                $model->gmirror1 = $value['gmirror1'];
                                $model->gmirror2 = $value['gmirror2'];
                                $model->md5 = $value['MD5'];
                                break;
                            case 5 :
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                $model->onedrive1 = $value['onedrive'];
                                $model->onedrive2 = $value['onedrivemirror2'];
                                $model->f = $value['1F'];
                                $model->gmirror1 = $value['gmirror1'];
                                $model->gmirror2 = $value['gmirror2'];
                                $model->md5 = $value['MD5'];
                                break;
                            case 6 :
                                $model->title = $value['Name'];
                                $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
                                $model->cat = $model->cat;
                                $model->url = $value['URL'];
                                break;

                        }
                        $model->save();
                        $new[] = $model->title;
                    }
/*exit;*/
                }
            }
           /* CastomController::printr($new);
            CastomController::printr($old);
            exit;*/
            @unlink($file);
            return $this->render('create', compact('model', 'old', 'new'));
        }

        return $this->render('create', [
            'model' => $model,
            'old' => $old,
            'new' => $new,
        ]);
    }

    /**
     * Updates an existing Games model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Games model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Games model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Games the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Games::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
