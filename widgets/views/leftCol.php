<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 26.02.2019
 * Time: 16:06
 */
use yii\helpers\Url;
$img = $user->getImage();
?>
<div class="col-md-3 static">
    <div class="profile-card">
        <?php if($img != null) :?>
            <img src="<?= $img->getUrl() ?>" alt="<?= $user->username ?>" class="profile-photo" />
        <?php else :?>
            <img src="http://placehold.it/300x300" alt="user" class="profile-photo">
        <?php endif; ?>
        <h5><a href="<?= Url::to(['/user/lk', 'id' => $user->id])?>" class="text-white"><?= $user->username ?></a></h5>
        <!-- <a href="#" class="text-white"><i class="ion ion-android-person-add"></i> 1,299 followers</a>-->
    </div><!--profile card ends-->
    <ul class="nav-news-feed">
        <li><i class="icon ion-ios-paper"></i><div><a href="<?= Url::to('/post') ?>">Посты</a></div></li>
        <li><i class="icon fa fa-gamepad" aria-hidden="true"></i><div><a href="<?= Url::to('/games') ?>">Игры</a></div></li>
        <li><i class="icon fa fa-user" aria-hidden="true"></i><div><a href="<?= Url::to(['/user/lk', 'id' => Yii::$app->user->id]) ?>">Профиль</a></div></li>
       <!-- <li><i class="icon ion-ios-people-outline"></i><div><a href="<?/*= Url::to('/user/friend') */?>">Друзья</a></div></li>-->
        <li><i class="icon ion-chatboxes"></i><div><a href="<?= Url::to('/user/interlocutor')?>">Сообщения</a></div></li>
        <li><i class="icon fa fa-sign-out" aria-hidden="true""></i><div><a href="<?= Url::to('/site/logout') ?>">Выход</a></div></li>
    </ul><!--news-feed links ends-->
    <!--<div id="chat-block">
        <div class="title">Chat online</div>
        <ul class="online-users list-inline">
            <li><a href="newsfeed-messages.html" title="Linda Lohan"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Sophia Lee"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="John Doe"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Alexis Clark"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="James Carter"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Robert Cook"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Richard Bell"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Anna Young"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
            <li><a href="newsfeed-messages.html" title="Julia Cox"><img src="http://placehold.it/300x300" alt="user" class="img-responsive profile-photo" /><span class="online-dot"></span></a></li>
        </ul>
    </div>--><!--chat block ends-->
</div>
