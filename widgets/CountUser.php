<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 24.03.2019
 * Time: 10:10
 */

namespace app\widgets;
use yii\base\Widget;
use app\models\User;
use Yii;

class CountUser extends Widget
{
    public function run()
    {
        $count = User::find()->count();;

        return $count;
    }
}