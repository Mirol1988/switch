<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 26.02.2019
 * Time: 16:05
 */

namespace app\widgets;


use yii\base\Widget;
use app\models\User;
use Yii;

class SiteLeftCol extends Widget
{
    public function run()
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        return $this->render('leftCol', compact('user'));
    }
}