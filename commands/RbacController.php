<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 25.02.2019
 * Time: 10:29
 */

namespace app\commands;
use yii\console\Controller;
use app\models\User;
use Yii;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        //Удалить старые записи из БД
        $auth->removeAll();

        //Создадим роли
        $user = $auth->createRole('user');
        $user->description = 'Пользователь';

        $moder = $auth->createRole('moder');
        $moder->description = 'Модератор';

        $redactor = $auth->createRole('redactor');
        $redactor->description = 'Редактор';

        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';

        //Записываем роли в БД
        $auth->add($user);
        $auth->add($moder);
        $auth->add($redactor);
        $auth->add($admin);


        // Дабавляем и описываем роли


        $AdminPage = $auth->createPermission('AdminPage');
        $AdminPage->description = 'Админки';

        $creatPost = $auth->createPermission('CreatPost');
        $creatPost->description = 'Добавление поста';

        $delitePost = $auth->createPermission('DelitePost');
        $delitePost->description = 'Удаление поста';

        $editPost = $auth->createPermission('EditPost');
        $editPost->description = 'Редактирование поста';

        $blocUsert = $auth->createPermission('BlocUsert');
        $blocUsert->description = 'Блокировка пользователя';

        $deliteUser = $auth->createPermission('DeliteUser');
        $deliteUser->description = 'Удаление пользователя';


        //Записать роли в БД
        $auth->add($AdminPage);
        $auth->add($creatPost);
        $auth->add($delitePost);
        $auth->add($editPost);
        $auth->add($blocUsert);
        $auth->add($deliteUser);


        // Присваеваем разрешения ролям
        $auth->addChild($user, $creatPost);
        $auth->addChild($user, $delitePost);
        $auth->addChild($user, $editPost);
        $auth->addChild($moder, $blocUsert);




        //Наследуем роли
        $auth->addChild($redactor, $user);
        $auth->addChild($moder, $redactor);
        $auth->addChild($admin, $moder);


        //Разрешения админа
        $auth->addChild($admin, $AdminPage);
        $auth->addChild($admin, $deliteUser);

        // Прописываем Админа и менеджера
        $auth->assign($admin, 1);
       /* $auth->assign($admin, 10);
        $auth->assign($manager, 11);
        $auth->assign($user, 2);*/

    }
}