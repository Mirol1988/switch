<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 26.03.2019
 * Time: 17:31
 */

namespace app\commands;
use yii\console\Controller;
use app\models\User;
use Yii;
use app\models\Post;

class VkController extends Controller
{

    function send()
    {
        $url = 'https://api.vk.com/method/wall.get';
        $params = array(
            'owner_id' => '-178659422',
            //'owner_id' => '-122341224', //switch open
            'count' => 99,
            'filter' => 'all',
            //'domain' => 'https://vk.com/nincfw',
            //'access_token' => 'a8c5f2f5f78c477fd6ee21708c09a97b4b9ebc0b7935d6d4f0b2b318c968df8e6fd20f79e1df22027f1c5',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            'access_token' => '16a2675f16a2675f16a2675fcd16cb1cc2116a216a2675f4a3583fe06b43d046f9ca1f7',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            //'access_token' =>  md5(api_id + '_' + viewer_id + '_' + api_secret),
            //'access_token' =>  md5('6912925' + '_' + '167914520' + '_' + 'O7B29s9BGShmOdp6ZH7o'),
            'v' => '5.37',
        );

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return $result;
    }
    public function actionVk()
    {



        $this->layout = false;
        $array = json_decode($this->send());


        $posts = array();
        foreach ($array as  $value){
            foreach ($value->items as $item){
                $posts[] = $item ;
            }
        }
        unset($posts[0]);

        $postSeve = array(); // масив для разбора поста
        $i = 0;
        foreach ($posts as $post){
            if($post->from_id == '-178659422'){
                $postSeve[$i]['id'] = $post->id ;
                $postSeve[$i]['date'] = date('Y-m-d H:i:s', $post->date);

                $postSeve[$i]['text'] = '<p>'.nl2br($post->text).'</p>';
                if(isset($post->attachments)){
                    foreach ($post->attachments as $attachments){
                        if($attachments->type == 'link'){
                            $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->link->url.'" >'.$attachments->link->title.'</a></p><p>';
                            //echo $postSeve[$i]['text'];
                            //CastomController::printr($attachments->link->photo->photo_604);
                        }
                        if($attachments->type == 'photo'){
                            $postSeve[$i]['photo'][] = $attachments->photo->photo_604;
                        }
                        if($attachments->type == 'doc'){
                            $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->doc->url.'">'.$attachments->doc->title.'</a></p>';
                        }
                    }

                }
                if(isset($post->copy_history)){
                    foreach ($post->copy_history as $copy_history){
                        $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p>'.nl2br($copy_history->text).'</p>';
                        if(isset($copy_history->attachments)){
                            foreach ($copy_history->attachments as $attachments){
                                if($attachments->type == 'link'){
                                    $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->link->url.'" >'.$attachments->link->title.'</a></p><p>';
                                    //echo $postSeve[$i]['text'];
                                    //CastomController::printr($attachments->link->photo->photo_604);
                                }
                                if($attachments->type == 'photo'){
                                    $postSeve[$i]['photo'][] = $attachments->photo->photo_604;
                                }
                                if($attachments->type == 'doc'){
                                    $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->doc->url.'">'.$attachments->doc->title.'</a></p>';
                                }
                            }

                        }
                    }
                    //CastomController::printr($post->copy_history);
                    //
                }

            }
            $i++;
            //$model = Post::find()->where(['id_vk' => $post->id]);

        }
        /*CastomController::printr($postSeve);
        exit;*/
        foreach ($postSeve as $save){
            $item = Post::find()->where(['vk_id' => $save['id']])->one();
            //echo
            //CastomController::printr($save['text']);
            if(!$item){
                $model = new Post();
                $model->text = $save['text'];
                $model->vk_id = $save['id'];
                $model->date = $save['date'];
                $model->id_user = 1;
                if($model->save()){
                    if(isset($save['photo'])){
                        foreach ($save['photo'] as $scrin) {
                            $img = $scrin;
                            if ($img) {
                                $nameFile = substr(md5(microtime() . rand(0, 9999)), 0, 20);
                                $localPath = \Yii::getAlias('@web') . '/img/tempImg/' . $nameFile . '.jpg';
                                //echo $localPath;
                                file_put_contents($localPath, file_get_contents($img));
                                chmod($localPath, 0777);  // восьмеричное, верный способ
                                $model->attachImage($localPath);
                                unset($localPath);
                            }
                        }
                       // CastomController::printr($model->text);
                    }
                }else {
                   // CastomController::printr($model->errors);
                }


            }
        }



        //cc6400e7577426322a5fce79c85120712849f86c250bf8406a45403b4f60ae88ac1b3cedc5f66b8ef8b36
//-178659422
        /**
         * https://oauth.vk.com/authorize?client_id=6912925&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=wall&response_type=token&v=5.37
         */

    }
}