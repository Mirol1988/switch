<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property int $id_post Пост
 * @property int $id_user Пользователь
 * @property string $text Текст сообщения
 * @property int $id_user_otv Пользователь каму ответели
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getOtvet() {
        return $this->hasOne(User::className(), ['id' => 'id_user_otv']);
    }

    public function getPost() {
        return $this->hasOne(Post::className(), ['id' => 'id_post']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post', 'id_user', 'text'], 'required'],
            [['id_user_otv', 'date'], 'safe'],
            [['id_post', 'id_user', 'id_user_otv'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post' => 'Пост',
            'id_user' => 'Пользователь',
            'text' => 'Текст сообщения',
            'id_user_otv' => 'Пользователь каму ответели',
        ];
    }

    public static function getUser($id){
        return $model = User::findOne($id);
    }
}
