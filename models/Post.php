<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $id_user Пользователь
 * @property string $text Текст поста
 * @property int $cat Раздел
 * @property int $like Лайк
 * @property int $dislike Дизлайк
 */
class Post extends \yii\db\ActiveRecord
{

    public $images; //Переменая для изображения
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }


    public function getComment() {
        return $this->hasMany(Comment::className(), ['id_post' => 'id'])->orderBy('date DESC')->limit(1);
    }

    public function getAllcomments(){
        return $this->hasMany(Comment::className(), ['id_post' => 'id'])->orderBy('date DESC');
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'text'], 'required'],
            [['cat', 'like', 'dislike','date', 'magnet', 'vk_id'], 'safe'],
            [['id_user', 'cat', 'like', 'dislike'], 'integer'],
            [['text', 'magnet'], 'string'],
            [['images'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxFiles' => 10, 'maxWidth' => 1920, 'maxHeight' => 1080],
        ];
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            /*'id' => 'ID',*/
            'id_user' => 'Пользователь',
            'text' => 'Текст поста',
            'cat' => 'Закрепить пост',
            'like' => 'Лайк',
            'dislike' => 'Дизлайк',
            'images' => 'Изображения',
            'magnet' => 'magnet ссылка',
        ];
    }

    /*
    * Загрузка нескольких картинок
    */
    public function uploadGallery()
    {
        if ( $this->validate() )
        {
            foreach ($this->images as $file)
            {
                $path = 'img/store' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path, false);
                @unlink($path);
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
