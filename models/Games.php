<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property int $id
 * @property string $title Название
 * @property int $cat Категория
 * @property string $date Дата выхода
 * @property string $url Ссылка
 * @property string $onedrive1 OneDrive
 * @property string $onedrive2 Зеркало OneDrive
 * @property string $gmirror1 Зеркало Google Drive 1
 * @property string $gmirror2 Зеркало Google Drive 2
 * @property string $untrimmed Необрезные
 * @property string $1f 1F
 * @property string $md5 Хеш сумма
 */
class Games extends \yii\db\ActiveRecord
{
    public $image;
    public $xlsx;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'cat', 'date', 'url'], 'required', 'on'=>'games'],
            [['cat'], 'integer'],
            [['onedrive1', 'onedrive2', 'gmirror1', 'gmirror2', 'untrimmed', 'f',  'md5', 'img', 'title'], 'safe'],
            [['title', 'url', 'onedrive1', 'onedrive2', 'gmirror1', 'gmirror2', 'untrimmed', 'f', 'md5', 'img'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png, jpg', 'skipOnEmpty' => true],
            [['xlsx'], 'file', 'extensions' => 'xlsx', 'skipOnEmpty' => true, 'on' => 'pars', 'checkExtensionByMimeType' => false],
            [['name',  'date', 'url'], 'safe' , 'on' => 'pars'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'cat' => 'Категория',
            'date' => 'Дата выхода',
            'url' => 'Ссылка',
            'onedrive1' => 'OneDrive',
            'onedrive2' => 'Зеркало OneDrive',
            'gmirror1' => 'Зеркало Google Drive 1',
            'gmirror2' => 'Зеркало Google Drive 2',
            'untrimmed' => 'Необрезные',
            'f' => '1F',
            'md5' => 'Хеш сумма',
            'image' => 'Изображение',
            'xlsx' => 'xlsx файл',
        ];
    }


    public function uploadFile()
    {
        if ($this->validate())
        {
            $name = Yii::$app->getSecurity()->generateRandomString(32);
            $path ='file/' . $name . '.' . $this->xlsx->extension;
            $this->xlsx->saveAs($path, false);
            return $path;
        }
        else
        {
            return false;
        }
    }


}
