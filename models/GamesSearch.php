<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Games;

/**
 * GamesSearch represents the model behind the search form of `app\models\Games`.
 */
class GamesSearch extends Games
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cat'], 'integer'],
            [['title', 'date', 'url', 'onedrive1', 'onedrive2', 'gmirror1', 'gmirror2', 'untrimmed', 'f', 'md5'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Games::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cat' => $this->cat,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'onedrive1', $this->onedrive1])
            ->andFilterWhere(['like', 'onedrive2', $this->onedrive2])
            ->andFilterWhere(['like', 'gmirror1', $this->gmirror1])
            ->andFilterWhere(['like', 'gmirror2', $this->gmirror2])
            ->andFilterWhere(['like', 'untrimmed', $this->untrimmed])
            ->andFilterWhere(['like', '1f', $this->f])
            ->andFilterWhere(['like', 'md5', $this->md5]);

        return $dataProvider;
    }
}
