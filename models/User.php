<?php

namespace app\models;
use app\controllers\CastomController;
use Yii;
use \yii\db\ActiveRecord;
use \yii\web\IdentityInterface;
use yii\helpers\Html;
use yii\helpers\Url;
/*use yii\behaviors\TimestampBehavior;
use yii\db\Expression;*/

class User extends ActiveRecord  implements IdentityInterface
{
    public $rememberMe = true;
    public $_user = false;
    public $image;


    const ACTIVE_USER = 1;
    const INVITE = 1; // Обычный инвайт
    const VIP_INVITE = 2; // vip инвайт
    public $role = array(); // Используется в форме редоктирования пользователя



    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function() { return date('U');},
            ],
        ];
    }





    public static function tableName()
    {
        return 'user';
    }



    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'required', 'on' => 'registration'],
            [['auth_key', 'code','active',  'rememberMe', 'invite', 'image'], 'safe', 'on' => 'registration'],
            ['password', 'match', 'pattern' => '#\d.*\d#s', 'message' => 'Пароль должен содержать минимум 2 буквы и 2 цифры.', 'on' => 'registration'],
            ['password', 'match', 'pattern' => '#[a-z].*[a-z]#is', 'message' => 'Минимум 2 буквы верхнего и нижнего регистра и 2 цифры.', 'on' => 'registration'],
            ['password', 'match', 'pattern' => '#[A-Z].*[A-Z]#is', 'message' => 'Минимум 2 буквы верхнего и нижнего регистра и 2 цифры.', 'on' => 'registration'],
            [['password'],'string', 'length' => [7, 255], 'on' => 'registration'],
            //[['username', ], 'exist', 'targetAttribute' => ['username'] ,'on' => 'registration'],

            [['active','created_at', 'updated_at', 'invite'], 'integer'],
            [['email', 'password', 'username','auth_key', 'code'], 'string', 'max' => 255],
            ['email', 'email'],

            [['email', 'password'], 'required', 'on' => 'login'],
            [['username','auth_key', 'code','active',  'rememberMe', 'invite', 'image'], 'safe', 'on' => 'login'],

            [['auth_key', 'code','active',  'rememberMe', 'invite', 'username', 'email', 'image'], 'safe', 'on' => 'edit'],
            [['image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'maxWidth' => 300, 'maxHeight' => 300],
            [['role'], 'safe', 'except' => ['redactor'], 'message' => 'У пользователя должна быть минимум одна роль'],
        ];
    }

    /**
     * @return mixed
     * Отправка почты с потверждением Email
     */
    public function sendCongirmationLink()
    {
        $confirmationLinkUrl = Url::to(['site/confirmemail', 'email' => $this->email, 'code' => $this->code]);
        $confirmationLink = Html::a('Потвердите Email', $confirmationLinkUrl);

        $sendingResult = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($this->email)
            ->setSubject('Потвердите Email')
            ->setHtmlBody('<p> Для прохождения регистрации Вам необходимо потвердить свой email</p>
            <p>'. $confirmationLink .'</p>')
            ->send();
        return $sendingResult;
    }




    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
            'username' => 'Ваш Ник',
            'image' => 'Аватар',
            'rememberMe' => 'Запомни меня',
            'role' => 'Права',
            'active' => 'Статус',
            'invite' => 'Доступ к разделу игр',
        ];
    }


    public static function findIdentity($id)
    {
        return static ::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    public static function findByUsername($email)
    {
        return static ::findOne(['email' => $email, 'active' => self::ACTIVE_USER]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function validatePassword($password)
    {
        if(Yii::$app->getSecurity()->validatePassword($password, $this->password)){
            return true;
        } else {
            return false;
        }
        //return Yii::$app->security->validatePassword($password, $this->password);
    }

    /*public function validatePassword($password){
        if(is_null($this->password))
            return CastomController::printr($this->);
        return Yii::$app->getSecurity()->validatePassword($this->salt . $password, $this->password);
    }*/

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /*public function login()
    {
        $this->scenario = 'login';


        if ($this->validate())
        {
            if ($this->rememberMe)
            {
                $cookie = $this->getUser();

                if($cookie != null){
                    $cookie->generateAuthKey();
                    $cookie->save();

                }
            }

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 *30 : 0);
        }
    }*/


    /*public function login()
    {
        $this->scenario = 'login';

        if ($this->validate())
        {
            if ($this->rememberMe)
            {
                $cookie = $this->getUser();
                $cookie->generateAuthKey();
                $cookie->save();
            }

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 *30 : 0);
        }
    }*/

    public function getUser()
    {
        if($this->_user === false)
        {
            $this->_user = $this->findByUsername($this->email);
        }

        return $this->_user;
    }

    public function getRoles(){
        $auth = Yii::$app->authManager;
        //Масив ролей пользователя
        $userRoles = $auth->getRolesByUser($this->id);
        return $userRoles;
    }

    /**
     * @return bool
     * Загрузка одного изображения
     */
    public function upload()
    {
        if ($this->validate())
        {
            $path = 'img/store' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
            $this->attachImage($path, true);
            @unlink($path);
            return true;
        }
        else
        {
            //return false;
            return $this->errors;
        }
    }


}
