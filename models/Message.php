<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $text
 * @property int $status
 * @property int $creatdate
 */
class Message extends \yii\db\ActiveRecord
{

    const ACTION_TO = 1;
    const ACTION_FROM = 2;
    public $images; //Переменая для изображения
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * @param $from
     * @param $to
     * @return array|\yii\db\ActiveRecord[]
     * Получаем сообщения
     */
    public static function findMessages($from, $to)
    {
        return self::find()
            ->where(['from' => $from, 'to' => $to])
            ->orWhere(['from'=>$to, 'to' => $from])
            ->all();
    }

    /**
     * @param $from
     * @param $to
     * @return array|\yii\db\ActiveRecord|null
     * Поиск последнего сообщения
     */
    public static function findLastMessage($from, $to){
        return self::find()
            ->where(['from' => $from, 'to' => $to])
            ->orWhere(['from'=>$to, 'to' => $from])
            /*->andWhere(['status' => 0])*/
            ->orderBy('creatdate DESC')
            ->one();
    }

   /* public static function findLastMessageActive($from, $to){
        return self::find()
            ->where(['from' => $from, 'to' => $to])
            ->andWhere(['status' => self::ACTION_TO])
            ->orderBy('creatdate DESC')
            ->one();
    }*/

    /**
     * @param $from
     * @param $to
     * @return int|string
     * Количество непрочитанных сообщений
     */
    public static function countMessage($from, $to){
        return self::find()
            ->where(['from' => $from, 'to' => $to])
            ->andWhere(['status' => 0])
            ->count();
    }



    /**
     * @param $from
     * @param $to
     * Смена статуса сообщения
     */
    public static function messageStatus($from, $to){
       $models = self::find()
           ->where(['from' => $from, 'to' => $to])
           ->andWhere(['status' => 0])
           ->all();

       foreach ($models as $model){
           $model->status = self::ACTION_TO;
           $model->creatdate = $model->creatdate;
           $model->save();
       }


    }


    public function getFromuser()
    {
        return $this->hasOne(User::className(),['id' => 'from'] );
    }
    public function getTouser()
    {
        return $this->hasOne(User::className(),['id' => 'to'] );
    }



    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'from']);
    }

    /*public function getTofrends() {
        return $this->hasOne(User::className(), ['id' => 'to']);
    }*/


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to', 'status', 'creatdate'], 'required'],
            [['text'], 'safe'],
            [['from', 'to', 'status'], 'integer'],
            ['text', 'string'],
            [['images'], 'image', 'extensions' => 'png, jpg, jpeg', 'skipOnEmpty' => true, 'maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'text' => 'Text',
            'status' => 'Status',
            'creatdate' => 'Creatdate',
        ];
    }

    public function uploadGallery()
    {
        if ( $this->validate() )
        {
            foreach ($this->images as $file)
            {
                $path = 'img/store' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $this->attachImage($path, false);
                @unlink($path);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function sendNewMessage($from, $to, $text)
    {
        $too = User::findOne($to);
        $fromm = User::findOne($from);;

        $sendingResult = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($too->email)
            ->setSubject('Вам пришло новое сообщение')
            ->setHtmlBody('<p>' .$fromm->username.' ' .$text .'</p>')
            ->send();

        return $sendingResult;
    }
}


