<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ocenka".
 *
 * @property int $id
 * @property int $id_post
 * @property int $id_user
 * @property int $like
 * @property int $dislike
 */
class Ocenka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocenka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post', 'id_user', 'like', 'dislike'], 'safe'],
            [['id_post', 'id_user', 'like', 'dislike'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_post' => 'Id Post',
            'id_user' => 'Id User',
            'like' => 'Like',
            'dislike' => 'Dislike',
        ];
    }
}
