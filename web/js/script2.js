'use strict'

//Preloader
var preloader = $('#spinner-wrapper');
$(window).on('load', function() {
    var preloaderFadeOutTime = 500;

    function hidePreloader() {
        preloader.fadeOut(preloaderFadeOutTime);
    }
    hidePreloader();
});

jQuery(document).ready(function($) {

    //Incremental Coutner
    if ($.isFunction($.fn.incrementalCounter))
        $("#incremental-counter").incrementalCounter();

    //For Trigering CSS3 Animations on Scrolling
    if ($.isFunction($.fn.appear))
        $(".slideDown, .slideUp").appear();

    $(".slideDown, .slideUp").on('appear', function(event, $all_appeared_elements) {
        $($all_appeared_elements).addClass('appear');
    });

    //For Header Appearing in Homepage on Scrolling
    var lazy = $('#header.lazy-load')

    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 200) {
            lazy.addClass('visible');
        } else {
            lazy.removeClass('visible');
        }
    });

    //Initiate Scroll Styling
    if ($.isFunction($.fn.scrollbar))
        $('.scrollbar-wrapper').scrollbar();


    if ($.isFunction($.fn.masonry)) {

        // fix masonry layout for chrome due to video elements were loaded after masonry layout population
        // we are refreshing masonry layout after all video metadata are fetched.
        var vElem = $('.img-wrapper video');
        var videoCount = vElem.length;
        var vLoaded = 0;

        vElem.each(function(index, elem) {

            //console.log(elem, elem.readyState);

            if (elem.readyState) {
                vLoaded++;

                if (count == vLoaded) {
                    $('.js-masonry').masonry('layout');
                }

                return;
            }

            $(elem).on('loadedmetadata', function() {
                vLoaded++;
                //console.log('vLoaded',vLoaded, this);
                if (videoCount == vLoaded) {
                    $('.js-masonry').masonry('layout');
                }
            })
        });


        // fix masonry layout for chrome due to image elements were loaded after masonry layout population
        // we are refreshing masonry layout after all images are fetched.
        var $mElement = $('.img-wrapper img');
        var count = $mElement.length;
        var loaded = 0;

        $mElement.each(function(index, elem) {

            if (elem.complete) {
                loaded++;

                if (count == loaded) {
                    $('.js-masonry').masonry('layout');
                }

                return;
            }

            $(elem).on('load', function() {
                loaded++;
                if (count == loaded) {
                    $('.js-masonry').masonry('layout');
                }
            })
        });

    } // end of `if masonry` checking


    //Fire Scroll and Resize Event
    $(window).trigger('scroll');
    $(window).trigger('resize');
});

/**
 * function for attaching sticky feature
 **/

function attachSticky() {
    // Sticky Chat Block
    $('#chat-block').stick_in_parent({
        parent: '#page-contents',
        offset_top: 70
    });

    // Sticky Right Sidebar
    $('#sticky-sidebar').stick_in_parent({
        parent: '#page-contents',
        offset_top: 70
    });

}

// Disable Sticky Feature in Mobile
$(window).on("resize", function() {

    if ($.isFunction($.fn.stick_in_parent)) {
        // Check if Screen wWdth is Less Than or Equal to 992px, Disable Sticky Feature
        if ($(this).width() <= 992) {
            $('#chat-block').trigger('sticky_kit:detach');
            $('#sticky-sidebar').trigger('sticky_kit:detach');

            return;
        } else {

            // Enabling Sticky Feature for Width Greater than 992px
            attachSticky();
        }

        // Firing Sticky Recalculate on Screen Resize
        return function(e) {
            return $(document.body).trigger("sticky_kit:recalc");
        };
    }
});

// Fuction for map initialization
function initMap() {
  var uluru = {lat: 12.927923, lng: 77.627108};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: uluru,
    zoomControl: true,
    scaleControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true
  });
  
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });
}

$( document ).ready(function() {
    var grid = document.querySelector('.grid');

    var msnry = new Masonry( grid, {
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });

    imagesLoaded( grid ).on( 'progress', function() {
        // layout Masonry after each image loads
        msnry.layout();
    });
});


$(document).ready(function(){

    var elems = $(".grid");
    var elemsTotal = elems.length;
    for(var i=0; i<elemsTotal; ++i){
        var grid = /*document.querySelector*/(elems[i]);

        var msnry = new Masonry( grid, {
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true
        });

        imagesLoaded( grid ).on( 'progress', function() {
            // layout Masonry after each image loads
            msnry.layout();
        });
    }
});





$(document).on('click', '.com-tol', function (e) {
    e.preventDefault();
    var id = $(this).data('post');
    var url = '/post/comment-create';
    var text = $('.t_'+id).val();
    var id_user = $('.u_'+id).val();
    var id_user_otv = $('.u_o_'+id).val();

    $.ajax({
        url: url,
        data: {id:id, text:text, id_user:id_user,  id_user_otv:id_user_otv},
        type: 'get',
        success: function (res) {
            //alert(res);
             if(!res) {
                 alert('ошибка');
             }else{
                 $('.c_'+id).append(anchorme(res,{
                     attributes:
                         [
                             {
                                 name:"target",
                                 value:"_blank",


                             },
                         ],
                     truncate:40
                 }));
                 $('.t_'+id).val('');
                 $('.u_o_'+id).val('0');
             }
        },
        error:function (res) {
            res = 'ошибка';
        }
    });
});



function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}

function setCaretToPos(input, pos) {
    setSelectionRange(input, pos, pos);
}

/**
 * Показать комментарии
 */
$(document).ready(function() {


    $('.post-comment p').each(function(i,elem) {
        //alert($(elem).text());
        $(elem).html(anchorme($(elem).html(),{
            attributes:
                [
                    {
                        name:"target",
                        value:"_blank",

                    },
                ],
            truncate:40,
        }));
        //console.log($(elem).text());
    });


    $(document).on('click', '.all-comments', function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            url: '/post/all-comment',
            data: {id:id},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res){

                }else{
                    $('.c_'+id).empty();
                    $('.c_'+id).addClass('animated fadeIn').append(anchorme(res,{
                        attributes:
                            [
                                {
                                    name:"target",
                                    value:"_blank",


                                },
                            ],
                        truncate:40
                    }));
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
        $(this).removeClass('all-comments').addClass('hidden-comments').empty().html('Скрыть комментарии');
    });

    function countComment(id){
        var theResponse = null;
        $.ajax({
            url: '/post/count-comment', // отпровляем в контролер Entry
            data: {id: id}, // передаём id
            dataType: "html",
            type: 'GET',   // отпровляем методом get
            async: false,

            // если хорошо то какийты действия
            success: function (res) {
                // если пользователь попытался подменить id то выводем в сообщение
                theResponse = res;
                //alert(theResponse);

            },
            // если ошибка то выведем ошибку
            error: function (res) {
            }
        });

        return theResponse;
    }


    /**
     * Скрыть комментарии
     */
    $(document).on('click', '.hidden-comments', function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            url: '/post/hidden-comment', // отпровляем в контролер Entry
            data: {id: id}, // передаём id
            type: 'GET',   // отпровляем методом get
            // если хорошо то какийты действия
            success: function (res) {
                // если пользователь попытался подменить id то выводем в сообщение
                $('.c_'+id).empty().removeClass('animated fadeIn').addClass('animated fadeIn').html(anchorme(res,{
                    attributes:
                        [
                            {
                                name:"target",
                                value:"_blank",


                            },
                        ],
                    truncate:40
                }));
                $('html, body').animate({ scrollTop: $('.foc_'+id).offset().top }, 'slow');

            },
            // если ошибка то выведем ошибку
            error: function (res) {
            }
        });
        var c = countComment(id);
        console.log(c);
        $(this).removeClass('hidden-comments').addClass('all-comments').html('Все комментарии (' + c + ')' );
    });




    $(document).on('click', '.com-com', function (e) {
        e.preventDefault();
        var id = $(this).data('com');
        var  id_user = $(this).data('user');
        var username = $(this).data('username');

        $('.u_o_'+id).val('');
        $('.u_o_'+id).val(parseInt(id_user));
        //$('.t_'+id).val('');
        $('.t_'+id).val($('.t_'+id).val() + '@'+username + ',' + ' ');
        //setCaretToPos($('.t_'+id)[0], $('.t_'+id).val().length);

        var caret = $('.t_'+id).getSelection().start;
        console.log(caret);
        var text = $('.t_'+id).val();
        console.log(text);
        // вставляем перенос строки
        $('.t_'+id).val(text.substring(0, caret)+(text.substring(caret)));
    });


    $(document).on('click', '.edit', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var post = $(this).data('post');

        $.ajax({
            url: '/post/com',
            data: {id:id},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res){

                }else{
                    var btn = $('.btn_'+post);
                    var response = res;
                    //var obj =  JSON.parse(response);
                    var obj =  response;
                    $('.u_o_'+post).val(obj.id_user_otv);
                    $('.t_'+post).val(obj.text);
                    $('.id_com_'+post).val(obj.id);
                    $('.date_'+post).val(obj.date);
                    btn.removeClass('com-tol').addClass('com-edit');
                    console.log(btn);
                    setCaretToPos($('.t_'+post)[0], $('.t_'+post).val().length);
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
    });

    $(document).on('click', '.com-edit', function (e) {

        e.preventDefault();

        var url = '/post/edit-comment';
        var post = $(this).data('post');
        var date = $('.date_'+post).val();
        var text = $('.t_'+post).val();
        var user =  $('.u_o_'+post).val();
        var btn = $('.btn_'+post);
        var id = $('.id_com_'+post).val();

        $.ajax({
            url: url,
            data: {id:id, text:text,  id_user_otv:user, post:post, date:date},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res) {
                    alert('ошибка');
                }else{
                    btn.removeClass('com-edit').addClass('com-tol');
                    $('.com_'+id).html('&nbsp;'+ anchorme(res,{
                        attributes:
                            [
                                {
                                    name:"target",
                                    value:"_blank",


                                },
                            ],
                        truncate:40
                    }));
                    //$('.t-e_'+id).removeAttr('data-text').attr('data-text',text);
                   // $('.c_'+id).append(res);
                    $('.t_'+post).val('');
                    $('.u_o_'+post).val('0');
                    $('.id_com_'+post).val('');
                    $('.date_'+post).val('');
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
    });

    $(document).on('click', '.delitecom', function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            url: '/post/del-com',
            data: {id:id},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res){

                }else{
                    $("div.post-comment_"+id).addClass('animated fadeOut');
                    setTimeout(function(){ $('div.post-comment_'+id).detach();},600)
                    //$("div.post-comment_"+id).remove();
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
    });


    $(document).on('click', '.jslike', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var like = $(this);

        $.ajax({
            url: '/post/like',
            data: {id:id},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res){

                }else{
                    like.html('<i class="icon ion-thumbsup"></i> '+res);
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
    });

    $(document).on('click', '.jsdislike', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var dislike = $(this);

        $.ajax({
            url: '/post/dislike',
            data: {id:id},
            type: 'get',
            success: function (res) {
                //alert(res);
                if(!res){

                }else{
                    dislike.html('<i class="fa fa-thumbs-down"></i> '+res);
                }
            },
            error:function (res) {
                res = 'ошибка';
            }
        });
    });

    $('.del').on('click', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var img = $(this).data('img');
        var url = '/post/del-img';
        //alert(id);

        $.ajax({
            url: url, // отпровляем в контролер Entry
            data: {id: id, img: img}, // передаём id
            type: 'GET',   // отпровляем методом get
            // если хорошо то какийты действия
            success: function (res) {
                // если пользователь попытался подменить id то выводем в сообщение
                if (res == 'ok'){

                    $('.del_'+img).remove();
                }
            },
            // если ошибка то выведем ошибку
            error: function (res) {

            }

        });
    });

});


jQuery('textarea').autoResize();

/*$('.emo').click(function(e) {
    e.preventDefault();
    $('#text-custom-trigger').emojiPicker('toggle');
});*/

$('.emo').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    //alert(id);
    $('.emodje_'+id).emojiPicker({
        width: '300px',
        height: '200px',
        button: false,
    });
    $('.emodje_'+id).emojiPicker('toggle');
    //console.log("emoji added, input val() is: " + $('.emodje_'+id).val());
});

$('.emo-mes').on('click', function (e) {
    e.preventDefault();
    $('.emodje').emojiPicker({
        width: '300px',
        height: '200px',
        button: false
    });
    $('.emodje').emojiPicker('toggle');
    //console.log("emoji added, input val() is: " + $('.emodje_'+id).val());
});


$('[data-fancybox="gallery"]').fancybox({
    loop: false,
    infobar : false,
    thumbs     : false,
    slideShow  : false,
    fullScreen : false,
    protect : true,
    arrows: false,
    wheel: false,
});



window.onload = function(){
    jQuery ('.mes'). scrollTop ( 9999999 );
    //document.getElementById('scroll').scrollTop = 9999;
}


/**
 * Отображение превью
 * @param evt
 */
function handleFileSelect(evt) {
    $('.tempImg').remove();
    var files = evt.target.files;

    for (var i = 0, f; i < files.length; i++) {
        f = files[i];
        if (!f.type.match('image.*')) continue;

        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {

                /*var img = $('<img />', {
                    class: 'thumb',
                    src: e.target.result,
                    title: escape(theFile.name)
                });*/


            var div1 ='<div class="col-sm-4 col-md-2 col-lg-2 del_3 tempImg">';
            var a1 ='<a href="#" class="thumbnail">';
            var img = '<img src="'+ e.target.result +'"  class="thumb">';
            var a2 = '</a>';
            var div2 = '</div>';


               // var img = '<img class="thumb img-responsive img-thumbnail"  src="'+e.target.result+'" title = " '+escape(theFile.name)+'"/>';
               // var div = '<div class="col-sm-6 col-md-4 col-lg-4 prevFoto">'
               // var endDiv ='</div>';
                //console.log(e.target.result);
                //$('#image_preview').append(div + img +endDiv);
                $('#image_preview').append(div1 + a1+ img +a2 +div2);

            };
        })(f);
        reader.readAsDataURL(f);
    }
}

/**
 * Вызов функции отоброжения превью
 */
$('#message-images').on('change', handleFileSelect);


$(document).on('click', '.delFotoPrev', function (e) {
    e.preventDefault();
    var files = $('#image_field');
    var name = $(this).data('name');

    var names = [];
    for (var i = 0; i < files.get(0).files.length; ++i) {

        if (files.get(0).files[i].name == name){
            //files.get(0).files[i].reset();
            console.log(files.get(0).files[i].value = '');
        }
        //names.push(files.get(0).files[i].name);
       // console.log(files.get(0).files[i]);
    }
    //$("input[name=file]").val(names);
    //console.log(names);
    $(this).parent('span').parent('a').parent('div').remove()
});


function breakText() {
    // определяем позицию курсора
    var caret = $('#message-text').getSelection().start;
    console.log(caret);
    var text = $('#message-text').val();
    console.log(text);
    // вставляем перенос строки
    $('#message-text').val(text.substring(0, caret)+'\r\n'+(text.substring(caret)));
    // Передвигаем курсор на новую строку.
    // Обратите внимание на fix в передаче параметра позиции курсора
    // функции setCaretPosition() для браузера Opera.
    //setCaretPosition('textarea', ($.browser.opera) ? caret+2 : caret+1);
}


function breakText_rangyinputs() {
    var caret = $('#message-text').getSelection().start;
    console.log(caret);
    $('#message-text').insertText('\r\n', caret, false).setSelection(caret+1, caret+1);
}

$('#message-text').bind('keydown', 'return', function() {
    $('#w0').submit();
    return false;
});

$('#message-text').bind('keydown', 'Ctrl+return', function() {
    //breakText_rangyinputs();
    breakText();
});

$(document).on('click', '.all-text', function (e) {
    e.preventDefault();
    var id = $(this).data('id');

    $.ajax({
        url: '/post/all-text', // отпровляем в контролер Entry
        data: {id: id}, // передаём id
        type: 'GET',   // отпровляем методом get
        // если хорошо то какийты действия
        success: function (res) {
            // если пользователь попытался подменить id то выводем в сообщение
                $('.text_'+id).empty().addClass('animated fadeIn').html(res);

        },
        // если ошибка то выведем ошибку
        error: function (res) {
        }
    });
    $(this).removeClass('all-text').addClass('hidden-text').html('Скрыть текст');
});


$(document).on('click', '.hidden-text', function (e) {
    e.preventDefault();
    var id = $(this).data('id');

    $.ajax({
        url: '/post/hidden-text', // отпровляем в контролер Entry
        data: {id: id}, // передаём id
        type: 'GET',   // отпровляем методом get
        // если хорошо то какийты действия
        success: function (res) {
            // если пользователь попытался подменить id то выводем в сообщение
            $('.text_'+id).empty().removeClass('animated fadeIn').addClass('animated fadeIn').html(res);
            $('html, body').animate({ scrollTop: $('.foc_'+id).offset().top }, 'slow');

        },
        // если ошибка то выведем ошибку
        error: function (res) {
        }
    });
    $(this).removeClass('hidden-text').addClass('all-text').html('Показать полностью');
});


/*

jQuery(function($) {
    var re = /((http|https|ftp):\/\/[a-zа-я0-9\w?=&.\/-;#~%-]+(?![a-zа-я0-9\w\s?&.\/;#~%"=-]*>))/g;
    function makeHTML(textNode) {
        var source = textNode.data;
        return source.replace(re, function() {
            var url = arguments[0];
            var a = $('<a></a>').attr({'onclick' : 'window.open(\'' + url + '\'); return false;','href': '#', 'target': '_blank'}).text(url);
            return url.match(/^https?:\/\/$/) ? url : $('<div></div>').append(a).html();
        });
    };
    function eachText(node, callback) {
        $.each(node.childNodes, function() {
            if (this.nodeType != 8 && this.nodeName != 'A') {
                this.nodeType != 1 ? callback(this) : eachText(this, callback);
            }
        });
    };
    $.fn.autolink = function() {
        return this.each(function() {
            var queue = [];
            eachText(this, function(e) {
                var html = makeHTML(e);
                if (html != e.data) {
                    queue.push([e, makeHTML(e)]);
                }
            });
            $.each(queue, function(i, x) {
                $(x[0]).replaceWith(x[1]);
            });
        });
    };
});


jQuery(function ($) {
    $('.chat-item').autolink();
});*/


    $('.post-text').each(function(i,elem) {
        //alert($(elem).text());
        $(elem).html(anchorme($(elem).html(),{
            attributes:
                [
                    {
                        name:"target",
                        value:"_blank",

                    },
                ],
            truncate:40
        }));
    });



tinymce.init({
    menubar:false,
    statusbar: false,
    image_dimensions: false,
});