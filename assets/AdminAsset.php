<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 18.03.2019
 * Time: 11:00
 */

namespace app\assets;

use yii\web\AssetBundle;
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}