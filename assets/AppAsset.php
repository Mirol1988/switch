<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       /* 'css/site.css',*/
        'css/bootstrap-theme.css',
        'css/emoji.css',
        'css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/jquery.mCustomScrollbar.css',
        'css/jquery.scrollbar.css',
        'css/jquery.emojipicker.css',
        'css/jquery.emojipicker.a.css',
        'css/emojione.picker.css',
        'css/style1.css',
        'css/jquery.fancybox.min.css',
        'css/animate.css',

    ];
    public $js = [
        'js/jquery.appear.min.js',
        'js/jquery.incremental-counter.js',
        'js/jquery.scrollbar.min.js',
        'js/jquery.sticky-kit.min.js',
        'js/masonry.pkgd.min.js',
        'js/jquery.fancybox.min.js',
        'js/imagesloaded.pkgd.min.js',
        'js/autoresize.jquery.js',
        'js/jquery.emojipicker.js',
        'js/jquery.emojis.js',
        'js/rangyinputs-jquery.js',
        'js/jquery.hotkeys.js',
        'js/anchorme.js',
        'js/script2.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
}
