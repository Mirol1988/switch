<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 23.02.2019
 * Time: 18:24
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>


<div class="col-sm-6 col-sm-offset-1">
    <div class="reg-form-container">

        <!-- Register/Login Tabs-->
        <div class="reg-options">
            <ul class="nav nav-tabs">
                <li ><a href="/registration">Регистрация</a></li>
                <li class="active" ><a href="/login">Вход</a></li>
            </ul><!--Tabs End-->
        </div>

        <!--Registration Form Contents-->
        <div class="tab-content">

            <div class="tab-pane active" id="login">
                <h3><?= $this->title ?></h3>
                <p class="text-muted">Вход в свой аккаунт</p>

                <!--Login Form-->
                <?php $form = ActiveForm::begin([
                    'options' => ['class' => 'form-inline'],
                ]) ?>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="my-email" class="sr-only">Email</label>
                            <?= $form->field($model, 'email')->input('email', ['placeholder' => 'Введите Email', 'class' => 'form-control input-group-lg'])->label(false) ?>
                            <!--<input id="my-email" class="form-control input-group-lg" type="text" name="Email" title="Enter Email" placeholder="Your Email"/>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="my-password" class="sr-only">Password</label>
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Введите пароль', 'class' => 'form-control input-group-lg'])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <?= $form->field($model, 'rememberMe')->checkbox([
                                'template' => "{input} {label}\n<div class=\"col-lg-6\">{error}</div>",
                            ]) ?>
                        </div>
                    </div>
                    <p><a href="#">Забыли пароль?</a></p>
                    <?= Html::submitButton('Авторизоваться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <!--<button class="btn btn-primary">Register Now</button>-->
                    <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
<?php
$css = <<< CSS

.navbar-fixed-top, .navbar-fixed-bottom {
            position: relative !important;
        }
CSS;
$this->registerCss($css);
?>
<!--<div class="col-lg-offset-1 col-lg-6"></div>-->