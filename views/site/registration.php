<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 23.02.2019
 * Time: 18:21
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="col-sm-6 col-sm-offset-1">
    <div class="reg-form-container">

        <!-- Register/Login Tabs-->
        <div class="reg-options">
            <ul class="nav nav-tabs">
                <li class="active"><a href="/registration" >Регистрация</a></li>
                <li ><a href="/login" >Вход</a></li>
            </ul><!--Tabs End-->
        </div>

        <!--Registration Form Contents-->
        <div class="tab-content">

            <div class="tab-pane active" id="register">
                <h3><?= $this->title ?></h3>
                <p class="text-muted"> Присоеденяйся к поклоникам Nintendo</p>

                <!--Register Form-->
                <?php $form = ActiveForm::begin([
                    'options' => ['class' => 'form-inline'],
                ]) ?>

                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="firstname" class="sr-only">Ник</label>
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Введите Ник', 'class' => 'form-control input-group-lg'])->label(false) ?>
                            <!--<input id="firstname" class="form-control input-group-lg" type="text" name="firstname" title="Enter first name" placeholder="First name"/>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="email" class="sr-only">Email</label>
                            <?= $form->field($model, 'email')->input('email', ['placeholder' => 'Введите Email', 'class' => 'form-control input-group-lg'])->label(false) ?>
                            <!--<input id="email" class="form-control input-group-lg" type="text" name="Email" title="Enter Email" placeholder="Your Email"/>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <label for="password" class="sr-only">Пароль</label>
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Введите пароль', 'class' => 'form-control input-group-lg'])->label(false) ?>
                            <!--<input id="password" class="form-control input-group-lg" type="password" name="password" title="Enter password" placeholder="Password"/>-->
                        </div>
                    </div>
                    <p><a href="<?= Url::to('/login') ?>">Уже есть аккаунт?</a></p>
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <!--<button class="btn btn-primary">Register Now</button>-->
                    <?php ActiveForm::end(); ?>

            </div>
            <!--Registration Form Contents Ends-->

            <!--Login-->

        </div>
    </div>
</div>


<?php
$css = <<< CSS

.navbar-fixed-top, .navbar-fixed-bottom {
            position: relative !important;
        }
CSS;
 $this->registerCss($css);
?>