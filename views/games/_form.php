<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cat')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'onedrive1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'onedrive2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gmirror1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gmirror2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'untrimmed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'f')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'md5')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
