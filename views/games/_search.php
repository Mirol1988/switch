<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GamesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'cat') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'onedrive1') ?>

    <?php // echo $form->field($model, 'onedrive2') ?>

    <?php // echo $form->field($model, 'gmirror1') ?>

    <?php // echo $form->field($model, 'gmirror2') ?>

    <?php // echo $form->field($model, 'untrimmed') ?>

    <?php // echo $form->field($model, '1f') ?>

    <?php // echo $form->field($model, 'md5') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
