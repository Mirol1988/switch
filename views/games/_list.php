<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 23.02.2019
 * Time: 23:22
 */
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="col-md-6 col-sm-6">
    <div class="friend-card">
        <?php if(isset($model->img)): ?>
            <img src="../<?= $model->img ?>" alt="<?= $model->name?>" class="img-responsive cover" />
        <?php else : ?>
            <img src="../images/Untitled-1.png" alt="<?= $model->title?>" class="img-responsive cover" />
        <?php endif; ?>
        <div class="card-info">
            <!--<img src="http://placehold.it/300x300" alt="user" class="profile-photo-lg" />-->
            <div class="friend-info">
                <!--<a href="#" class="pull-right text-green">My Friend</a>-->
                <h5><?= Yii::$app->params['gamesCat'][$model->cat] ?></h5>
                <p><a href="<?= Url::to(['games/view', 'id' => $model->id]) ?>" class="profile-link"><?= \app\controllers\CastomController::name($model->title) ?></a></p>
            </div>
        </div>
    </div>
</div>
