<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\controllers\CastomController;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Игры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="games-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php if(Yii::$app->user->can('redactor')) :?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить игру?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php endif;?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'attribute' => 'cat',
                'value' => function ($data){
                    return Yii::$app->params['cat'][$data->cat];
                },
                'format' => 'html',
                /*'filter'=>Yii::$app->params['cat'],*/
                /*'headerOptions' => ['width' => '200'],*/
            ],
           /* 'date',*/
            [
                'attribute' => 'url',
                'value' => function ($data){
                    if($data->url != null){
                        return '<a href="'.$data->url.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'onedrive1',
                'value' => function ($data){
                    if($data->onedrive1 != null){
                        return '<a href="'.$data->onedrive1.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'onedrive2',
                'value' => function ($data){
                    if($data->onedrive2 != null){
                        return '<a href="'.$data->onedrive2.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'gmirror1',
                'value' => function ($data){
                    if($data->gmirror1 != null){
                        return '<a href="'.$data->gmirror1.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'gmirror2',
                'value' => function ($data){
                    if($data->gmirror2 != null){
                        return '<a href="'.$data->gmirror2.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'untrimmed',
                'value' => function ($data){
                    if($data->untrimmed != null){
                        return '<a href="'.$data->untrimmed.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            [
                'attribute' => 'f',
                'value' => function ($data){
                    if($data->f != null){
                        return '<a href="'.$data->f.'"><i class="glyphicon glyphicon-download-alt"></i></a>';
                    }else{
                        return '';
                    }
                },
                'format' => 'html',
                'filter' => false,
            ],
            'md5',
            [
                'attribute' => 'Описание',
                'value' => function ($data){
                    return '<a href="https://www.nintendo.ru/-/--299117.html?q='.CastomController::name($data->title).'&f=147393" target="_blank">'.CastomController::name($data->title).'</a>';
                },
                'format' => 'raw',
                'filter' => false,
            ]
        ],
    ]) ?>

</div>
