<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\widgets\SiteLeftCol;

if(Yii::$app->user->isGuest){
    return Yii::$app->response->redirect(Url::to('/site/login'));
    //return $this->redirect(['site/login']);
}

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="content-type" content="text/html" charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <meta name="robots" content="index, follow" />
    <title><?= Html::encode($this->title) ?></title>

    <!-- Stylesheets
================================================= -->


    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">

    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="../images/FhNkvIgL.png"/>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Header
================================================= -->
<header id="header">
    <nav class="navbar navbar-default navbar-fixed-top menu">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>-->
                <a class="navbar-brand" href="/"><p class="text-white"><img  class="img-circle" style="width: 45px; height: 45px" src="../images/FhNkvIgL.png" alt="Custom Nintendo" /> Custom Nintendo</p></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <!-- <ul class="nav navbar-nav navbar-right main-menu">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span><img src="images/down-arrow.png" alt="" /></span></a>
                        <ul class="dropdown-menu newsfeed-home">
                            <li><a href="index.html">Landing Page 1</a></li>
                            <li><a href="index-register.html">Landing Page 2</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Newsfeed <span><img src="images/down-arrow.png" alt="" /></span></a>
                        <ul class="dropdown-menu newsfeed-home">
                            <li><a href="newsfeed.html">Newsfeed</a></li>
                            <li><a href="newsfeed-people-nearby.html">Poeple Nearly</a></li>
                            <li><a href="newsfeed-friends.html">My friends</a></li>
                            <li><a href="newsfeed-messages.html">Chatroom</a></li>
                            <li><a href="newsfeed-images.html">Images</a></li>
                            <li><a href="newsfeed-videos.html">Videos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Timeline <span><img src="images/down-arrow.png" alt="" /></span></a>
                        <ul class="dropdown-menu login">
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="timeline-about.html">Timeline About</a></li>
                            <li><a href="timeline-album.html">Timeline Album</a></li>
                            <li><a href="timeline-friends.html">Timeline Friends</a></li>
                            <li><a href="edit-profile-basic.html">Edit: Basic Info</a></li>
                            <li><a href="edit-profile-work-edu.html">Edit: Work</a></li>
                            <li><a href="edit-profile-interests.html">Edit: Interests</a></li>
                            <li><a href="edit-profile-settings.html">Account Settings</a></li>
                            <li><a href="edit-profile-password.html">Change Password</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle pages" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Pages <span><img src="images/down-arrow.png" alt="" /></span></a>
                        <ul class="dropdown-menu page-list">
                            <li><a href="index.html">Landing Page 1</a></li>
                            <li><a href="index-register.html">Landing Page 2</a></li>
                            <li><a href="newsfeed.html">Newsfeed</a></li>
                            <li><a href="newsfeed-people-nearby.html">Poeple Nearly</a></li>
                            <li><a href="newsfeed-friends.html">My friends</a></li>
                            <li><a href="newsfeed-messages.html">Chatroom</a></li>
                            <li><a href="newsfeed-images.html">Images</a></li>
                            <li><a href="newsfeed-videos.html">Videos</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="timeline-about.html">Timeline About</a></li>
                            <li><a href="timeline-album.html">Timeline Album</a></li>
                            <li><a href="timeline-friends.html">Timeline Friends</a></li>
                            <li><a href="edit-profile-basic.html">Edit Profile</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="contact.html">Contact</a></li>
                </ul>-->
                <?php if (in_array(Yii::$app->controller->route, Yii::$app->params['searchGames'])) {
                    $url = '/games';
                    $name = "GamesSearch[title]";
                }
                    if (in_array(Yii::$app->controller->route, Yii::$app->params['searchPost'])) {
                    $url = '/post';
                    $name = "PostSearch[text]";
                }

                if(!isset($url)){
                    $url = '/post';
                    $name = 'PostSearch[text]';
                }
                ?>
                <form class="navbar-form navbar-right hidden-sm" method="get" action="<?= $url ?>">
                    <div class="form-group">
                        <i class="icon ion-android-search"></i>

                        <input type="text" name="<?=$name?>" class="form-control" placeholder="Поиск">
                    </div>
                </form>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</header>
<!--Header End-->


<div id="page-contents">
    <div class="container">
        <div class="row">

            <!-- Newsfeed Common Side Bar Left
      ================================================= -->
            <?= SiteLeftCol::widget() ?>

            <div class="col-md-7">

                <!-- Post Create Box
                ================================================= -->
                <div class="create-post">
                    <div class="row">
                        <!--<div class="col-md-7 col-sm-7">
                            <div class="form-group">
                                <img src="http://placehold.it/300x300" alt="" class="profile-photo-md">
                                <textarea name="texts" id="exampleTextarea" cols="30" rows="1" class="form-control" placeholder="Write what you wish"></textarea>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="tools">
                                <ul class="publishing-tools list-inline">
                                    <li><a href="#"><i class="ion-compose"></i></a></li>
                                    <li><a href="#"><i class="ion-images"></i></a></li>
                                    <li><a href="#"><i class="ion-ios-videocam"></i></a></li>
                                    <li><a href="#"><i class="ion-map"></i></a></li>
                                </ul>
                                <button class="btn btn-primary pull-right">Publish</button>
                            </div>
                        </div>-->
                        <?php if (in_array(Yii::$app->controller->route, Yii::$app->params['searchGames'])) :?>
                            <a href="<?=Url::to('/games/create')?>" class="btn btn-primary">Добавить игру</a>
                        <?php elseif(in_array(Yii::$app->controller->route, Yii::$app->params['searchPost'])) :?>
                            <a href="<?=Url::to('/post/create')?>" class="btn btn-primary">Создать пост</a>
                        <?php endif; ?>

                    </div>
                </div>
                <!-- Friend List
                ================================================= -->
                <div class="friend-list">
                    <div class="row">
                        <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </div>
            </div>

            <!-- Newsfeed Common Side Bar Right
      ================================================= -->
            <div class="col-md-2 static">
                <div class="suggestions" id="sticky-sidebar">

                <?php if (in_array(Yii::$app->controller->route, Yii::$app->params['searchGames'])) :?>
                    <h4 class="grey"><a href="<?= \yii\helpers\Url::to('/games') ?>">Все категории</a></h4>
                    <h4 class="grey">Nintendo</h4>
                    <?php foreach (Yii::$app->params['cat'] as $key=> $value ) : ?>
                        <div class="follow-user">
                            <!--<img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />-->
                                <div>
                                    <h5><a href="<?= \yii\helpers\Url::to(['/games', 'GamesSearch[cat]' => $key]) ?>"><?= $value ?></a></h5>
                                    <!-- <a href="#" class="text-green">Add friend</a>-->
                                </div>
                        </div>

                    <?php endforeach; ?>
                    <!--<h4 class="grey">Sony</h4>-->
                    <?php /*foreach (Yii::$app->params['ps4'] as $key=> $value ) : */?><!--
                        <div class="follow-user">
                            <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm pull-left" />-->
                                <div>
                                    <h5><a href="<?/*= \yii\helpers\Url::to(['/games', 'GamesSearch[cat]' => $key]) */?>"><?/*= $value */?></a></h5>
                                    <!-- <a href="#" class="text-green">Add friend</a>-->
                                </div>
                        </div>

                    <?php /*endforeach; */?>

                <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


</div>

<!-- Footer
================================================= -->
<footer id="footer">
    <div class="container">
       <!-- <div class="row">
            <div class="footer-wrapper">
                <div class="col-md-3 col-sm-3">
                    <a href=""><img src="images/logo-black.png" alt="" class="footer-logo" /></a>
                    <ul class="list-inline social-icons">
                        <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-2">
                    <h5>For individuals</h5>
                    <ul class="footer-links">
                        <li><a href="">Signup</a></li>
                        <li><a href="">login</a></li>
                        <li><a href="">Explore</a></li>
                        <li><a href="">Finder app</a></li>
                        <li><a href="">Features</a></li>
                        <li><a href="">Language settings</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-2">
                    <h5>For businesses</h5>
                    <ul class="footer-links">
                        <li><a href="">Business signup</a></li>
                        <li><a href="">Business login</a></li>
                        <li><a href="">Benefits</a></li>
                        <li><a href="">Resources</a></li>
                        <li><a href="">Advertise</a></li>
                        <li><a href="">Setup</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-2">
                    <h5>About</h5>
                    <ul class="footer-links">
                        <li><a href="">About us</a></li>
                        <li><a href="">Contact us</a></li>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="">Terms</a></li>
                        <li><a href="">Help</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-3">
                    <h5>Contact Us</h5>
                    <ul class="contact">
                        <li><i class="icon ion-ios-telephone-outline"></i>+1 (234) 222 0754</li>
                        <li><i class="icon ion-ios-email-outline"></i>info@thunder-team.com</li>
                        <li><i class="icon ion-ios-location-outline"></i>228 Park Ave S NY, USA</li>
                    </ul>
                </div>
            </div>
        </div>-->
    </div>
    <!--<div class="copyright">
        <p>Thunder Team © 2016. All rights reserved</p>
    </div>-->
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
