<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\CountUser;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="content-type" content="text/html" charset="<?= Yii::$app->charset ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <meta name="robots" content="index, follow" />
    <title><?= Html::encode($this->title) ?></title>

    <!-- Stylesheets

    <!--Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">

    <!--Favicon-->
    <link rel="shortcut icon" type="image/png" href="../images/FhNkvIgL.png"/>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- Header
================================================= -->
<header id="header-inverse">
    <nav class="navbar navbar-default navbar-fixed-top menu">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><h1 class="text-white"><img  class="img-circle" style="width: 45px; height: 45px" src="../images/FhNkvIgL.png" alt="Custom Nintendo" /> Custom Nintendo</h1></a>
                <!--<a class="navbar-brand" href="index-register.html"><h1 class="text-white">Custom Nintendo</h1></a>-->
            </div>
        </div><!-- /.container -->
    </nav>
</header>
<!--Header End-->

<!-- Landing Page Contents
================================================= -->



<div id="lp-register">
    <div class="container wrapper">
        <div class="row">
            <div class="col-sm-5">
                <div class="intro-texts">
                    <h1 class="text-white">У нас вы сможете!!!</h1>
                    <p>Узнать обо всём интересном, что творится в мире открытого ПО на консолях от Nintendo, научитесь получать от своей приставки чуть больше, чем привыкли, а так же сможете получить ответы на интересующие вас вопросы. <br /> <!--<br />Why are you waiting for? Buy it now.--></p>
                    <!--<button class="btn btn-primary">Learn More</button>-->
                    <div id="incremental-counter" data-value="<?= CountUser::widget() ?>"></div>
                    <h2 class="text-white">уже с нами. Присоединяйтесь!</h2>
                </div>
            </div>
            <?= Alert::widget() ?>
            <?= $content ?>

        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-6">

                <!--Social Icons-->
                <!--<ul class="list-inline social-icons">
                    <li><a href="#"><i class="icon ion-social-facebook"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-twitter"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-googleplus"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-pinterest"></i></a></li>
                    <li><a href="#"><i class="icon ion-social-linkedin"></i></a></li>
                </ul>-->
            </div>
        </div>
    </div>
</div>

<!--preloader-->
<!--<div id="spinner-wrapper">
    <div class="spinner"></div>
</div>
-->
<!-- Scripts
================================================= -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>