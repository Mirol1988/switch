<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 11.03.2019
 * Time: 21:21
 */?>


<?php
$script = <<< JS
/*function updateList() {
      $.pjax.reload({container : '#list-messages'});
    }
    setInterval(updateList, 3000);*/
    
    /**
    * Проверка на непрочитаные сообщения
    */
    function playAudio(){
        var myAudio = new Audio;
        myAudio.src = "/saund/Sound_11154.wav";
        myAudio.play();
    }
    function show()  
        {
            var id = $('.chat-message').data('id');
            //alert (id);
            $.ajax({  
                url: "/user/right",  
                cache: false, 
                data: {id:id}, 
                success: function(html){  
                    if(html){
                        
                        if(html != 0){
                            console.log(0);
                            playAudio();
                            $(".chat-message").append(anchorme(html,{
                            attributes:
                            [
                                {
                                    name:"target",
                                    value:"_blank",
                                   
                                    
                                },
                            ],
                            truncate:40
                        })); 
                        jQuery ('.mes'). scrollTop ( 9999999 );
                        }
                        
                    }
                }  
            }); 
            $('[data-fancybox="gallery"]').fancybox({
                loop: false,
                infobar : false,
                thumbs     : false,
                slideShow  : false,
                fullScreen : false,
                protect : true,
                arrows: false,
                wheel: false,
            });
        }  
        
        function online(){
        var id = $('.online').data('id');
            //alert (id);
            $.ajax({  
                url: "/user/online",  
                cache: false, 
                data: {id:id}, 
                success: function(html){  
                    if(html){
                        
                        if(html != 0){
                            $(".online").addClass('green').html('Онлайн'); 
                        //jQuery ('.mes'). scrollTop ( 9999999 );
                        }else{
                           $(".online").removeClass('green').html('Офлайн');  
                        }
                        
                    }
                }  
            }); 
          
        }  
        online();
        /**
        *  Вызывем функцию проверки непрочитаных сообщений
        */
        $(document).ready(function(){  
            //show();  
            setInterval(show,10000);
            setInterval(online,20000);
        });
    
        $('[data-fancybox="gallery"]').fancybox({
        loop: false,
        infobar : false,
        thumbs     : false,
        slideShow  : false,
        fullScreen : false,
        protect : true,
        arrows: false,
        wheel: false,
});
    function refresh(){
        /*$('.chat-item p').each(function(i,elem) {
		//alert($(elem).text());
		$(elem).html(anchorme($(elem).text()));
        });*/
        /*val = document.getElementById("input").value;
        document.getElementById("output").innerHTML = anchorme(val);*/
       var val =  $('#message-text').val();
       $('#image_preview').append(anchorme(val));
    }
      /**
      *  Отправка сообщение ajax
      */
      $( '.addMes' ).on('beforeValidate', function( e ) {
          e.preventDefault();
          e.stopImmediatePropagation();
          var userId = $('#message-to').val();
          
          var action = $('#w0').attr('action');
          var url = '';
          if(action == '/user/message?id='+userId){
              url = '/user/addcom';
          } else if(action == '/user/save-edit-mess?id='+userId){
              url = '/user/seve-edit-mess';
          }
          
          console.log( $('#w0').attr('action'));
          //console.log( new FormData( this ));
            $.ajax( {
              url: url,
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              success: function(html){  
                    if(html){
                         if(action == '/user/message?id='+userId){
                                $(".chat-message").append(anchorme(html,{
                                    attributes:
                                    [
                                        {
                                            name:"target",
                                            value:"_blank",
                                           
                                            
                                        },
                                    ],
                                    truncate:40
                                })); 
                                jQuery ('.mes'). scrollTop ( 9999999 );
                                $('#message-text').val('');
                                $('#message-text').height('20');
                                $("#message-images").val(null);
                                $('#image_preview').empty();
                            } else if(action == '/user/save-edit-mess?id='+userId){
                                var idM = $('#message-id').val();
                                var response = html;
                                $('#w0').attr('action', '/user/message?id='+userId);
                                //$('.m_'+idM).empty();
                                
                                var divOpen = 
                                    '<div class="chat-item-header">'+
                                        '<h5>'+response.name+'</h5>'+
                                        '<small class="text-muted">'+
                                            '<a href="#" class="btn text-primary edit-mes" data-id="'+idM+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>'+
                                            '<a href="#" class="btn text-danger del-mes" data-id="'+idM+'"><i class="fa fa-trash" aria-hidden="true"></i></a>'+
                                                response.date +                                       
                                        '</small>'+
                                    '</div>'+
                                    '<p>'+response.text+'</p>';
                                var image ='';
                                var gal = response.gal;
                                if(gal){
                                    
                                      $.each(gal,function(i,elem) {
                                          
                                          image = image + '<a data-fancybox="gallery" class="gal img-responsive" href="'+elem['full']+'">'+
                                                    '<img class="img-responsive img-gallary post-image" src="'+elem['mini']+'" alt="">'+
                                                '</a>';
                                      });
                                }
                                
                                
                              
                                
                                $('.m_'+idM).html(divOpen + image);
                                
                                $('#message-text').val('');
                                $('#message-text').height('20');
                                $("#message-images").val(null);
                                $('#image_preview').empty();
                                $('#w0').attr('action', '/user/message?id='+userId);
                                
                            }
                        
                        //refresh();
                    }
                }  
            });
        return false;
      } );
      
      
      /*$( '#message-text' ).on('change', function( e ) {
          e.preventDefault();
          e.stopImmediatePropagation();
          var val = $(this).val();
          if(anchorme(val, {
                emails:false,
                urls:false,
                ips:false,
                files:false
            })){
              $('#url_preview').append(anchorme(val));
            }
           /!*$('#url_preview').append(anchorme(val));*!/
        return false;
      } );*/
      /**
      * Удаление сообщения
      */
      $(document).on('click', '.del-mes', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            
            $.ajax({
                url: '/user/del-mes',
                data: {id:id},
                type: 'get',
                success: function (res) {
                    //alert(res);
                    if(res == 'ok'){
                        $('.mess_'+id).addClass('animated fadeOut');
                        setTimeout(function(){ $('.mess_'+id).detach();},600)
                    }
                },
                error:function (res) {
                    res = 'ошибка';
                }
            });
      });
      

    $('.chat-item p').each(function(i,elem) {
		//alert($(elem).text());
		$(elem).html(anchorme($(elem).html(),{
                            attributes:
                            [
                                {
                                    name:"target",
                                    value:"_blank",
                                                                 
                                },
                            ],
                            truncate:40
                        }));
    });
    

    //document.getElementById("input").value = initialInput;
    //refresh();

    /*anchor me*/
    
    function breakTextMes() {
    // определяем позицию курсора
    var caret = $('#message-text').getSelection().start;
    console.log(caret);
    var text = $('#message-text').val();
    console.log(text);
    // вставляем перенос строки
    $('#message-text').val(text.substring(0, caret)+(text.substring(caret)));
    // Передвигаем курсор на новую строку.
    // Обратите внимание на fix в передаче параметра позиции курсора
    // функции setCaretPosition() для браузера Opera.
    //setCaretPosition('textarea', ($.browser.opera) ? caret+2 : caret+1);
}
    
    /**
    *  Редактирование сообщения
    */
   $(document).on('click', '.edit-mes', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            
            var userId = $('#message-to').val();
            $.ajax({
                url: '/user/edit-mes',
                data: {id:id},
                type: 'get',
                success: function (res) {
                    //alert(res);
                   var response = res;
                   $('#message-text').val(response.text).focus();
                   //breakTextMes();
                   //$('#w0').removeClass('addMes').addClass('editMes');
                   $('#message-id').val(id);
                   $('#w0').attr('action', '/user/save-edit-mess?id='+userId);
                   var gal = response.gal;
                   if(gal){
                      $.each(gal,function(i,elem) {
                        $('#image_preview').append(
                            '<div class="col-sm-4 col-md-2 col-lg-2 delIMGMes_'+i+'">'+
                                '<a href="#" class="thumbnail">'+
                                    '<img src="'+elem+'" alt="">'+
                                    '<span><i class="fa fa-times-circle del fa-lg delFoto" data-id="'+id+'" data-img="'+i+'"></i></span>'+
                                '</a>'+
                            '</div>'
                        );
                    });
                   }
                },
                error:function (res) {
                    res = 'ошибка';
                }
            });
            $(this).html('<i class="fa fa-times" aria-hidden="true"></i>');
            $(this).removeClass('edit-mes').addClass('clouse-edit');
      });
   
   
   /**
   *  отмена редактирования сообщения
    */
   $(document).on('click', '.clouse-edit', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var userId = $('#message-to').val();
            $(this).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
            $('#message-text').val('');
            $('#message-text').height('20');
            $("#message-images").val(null);
            $('#image_preview').empty();
            $('#message-id').val('0');
            $('#w0').attr('action', '/user/message?id='+userId);
            $(this).removeClass('clouse-edit').addClass('edit-mes');
      });
      
   /**
   *  Удаление фото при редоктировании
   */
   $(document).on('click', '.delFoto', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var img = $(this).data('img');

    $.ajax({
        url: '/user/del-img', // отпровляем в контролер Entry
        data: {id: id, img:img}, // передаём id
        type: 'GET',   // отпровляем методом get
        // если хорошо то какийты действия
        success: function (res) {
            // если пользователь попытался подменить id то выводем в сообщение
            console.log(res);
                //$('.text_'+id).empty().addClass('animated fadeIn').html(res);
                if(res == 'ok'){
                    $('.delIMGMes_'+img).detach();
                }

        },
        // если ошибка то выведем ошибку
        error: function (res) {
        }
    });
   // $(this).removeClass('all-text').addClass('hidden-text').html('Скрыть текст');
});
JS;

$this->registerJs($script);
?>