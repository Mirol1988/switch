<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 05.03.2019
 * Time: 14:15
 */
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Message;

//$this->title = 'Переписка: ';
//$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
/*$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$script = <<< JS
 /*function updateList() {
     $.pjax.reload({container : '#list-messages'});
    }
    setInterval(updateList, 1000);*/
function spisok(){
    
        var array = [];
        $('li.spisok').each(function(i,elem) {
            array.push($(elem).data('id'));
        });
        return array;
}
/*spisok();*/

/*var a = spisok();
console.log(a);*/

function playAudio(){
        var myAudio = new Audio;
        myAudio.src = "/saund/Sound_11154.wav";
        myAudio.play();
    }
function show()  
{
            var array = [];
                $('li.spisok').each(function(i,elem) {
                    array.push($(elem).data('id'));
                });
               // console.log(array);
            //alert (id);
            $.ajax({  
                type: "POST",
                url: "/user/spisok",  
                cache: false, 
                //data: {array:array}, 
                /*dataType: "json",*/
                success: function(html){  
                    if(html){
                        $.each(html, function(i, object) {
                            var elem = $('ul.contact-list');
                            if ($('.spisok_'+object.id).length > 0) {
                                var count = '<div class="chat-alert alert_'+object.id+'">'+object.count+'</div>';
                                if($('.alert_'+object.id).length > 0){
                                    console.log($('.alert_'+object.id).length);
                                    if($('.alert_'+object.id).html() < object.count){
                                        $('.alert_'+object.id).html(object.count);
                                        $('.text_'+object.id).html(object.message);
                                        playAudio();
                                    }
                                    if(object.status == 1){
                                        $('.status_'+object.id).html('Онлайн');
                                    }else{
                                         $('.status_'+object.id).removeClass('green').html('Офлайн');
                                    }
                                }else{
                                    $('.preview_'+object.id).append(count);
                                }
                            } else {
                                 if(object.status == 1){
                                     var status = '<small class="text-muted online green status_'+object.id+'">Онлайн</small>';
                                 }else{
                                     var status = '<small class="text-muted online  status_'+object.id+'">Офлайн</small>';
                                 }
                                 elem.append('<li class="spisok spisok_'+object.id+'">'+
                                    
                                                '<a href="/user/message?id='+object.id+'" data-toggle="tab">'+
                                                    '<div class="contact">'+
                        	                            '<img src="'+object.img+'" alt="'+object.name+'" class="profile-photo-sm pull-left">'+
                                                        '<div class="msg-preview preview_'+object.id+'">'+
                        		                            '<h6>'+object.name+'</h6>'+
                                                            '<p class="text-muted text_'+object.id+'">'+object.message+'</p>'+
                                                    
                                                            status +
                                                            '<div class="chat-alert">'+object.count+'</div>'+
                        	                            '</div>'+
                                                    '</div>'+
                                                '</a>'+
                                         '</li>'
                                 );
                                  playAudio();
                            }
                        });
                    };
                }
            });
}  

setInterval(show, 1000);
            
JS;

$this->registerJs($script);



?>


<div class="chat-room">
    <div  class="row">
        <div class="col-md-12">

            <div class="tab-content scrollbar-wrapper wrapper scrollbar-outer">
                <?php /*Pjax::begin([
                    'id' => 'list-messages',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'formSelector' => false,
                ]) */?>
                <ul class="nav nav-tabs contact-list">


                    <?php  foreach ($interlocutors as $interlocutor) :?>
                    <?php $img = $interlocutor->getImage(); ?>
                    <li  class="spisok spisok_<?=$interlocutor->id ?>" data-id="<?= $interlocutor->id ?>">
                        <a href="<?= Url::to(['user/message', 'id' => $interlocutor->id]) ?>" data-toggle="tab">
                            <div class="contact">
                                <?php if($img != null) :?>
                                    <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $interlocutor->user->username ?>" class="profile-photo-sm pull-left" />
                                <?php else :?>
                                    <img src="http://placehold.it/300x300" alt="<?= $interlocutor->user->username ?>" class="profile-photo-sm pull-left" />
                                <?php endif; ?>
                                <div class="msg-preview preview_<?= $interlocutor->id ?>">
                                    <h6><?= $interlocutor->username ?></h6>
                                    <?php
                                    $lastMessage = Message::findLastMessage($interlocutor->id, Yii::$app->user->id);

                                   // $lastMessageActive = Message::findLastMessageActive($interlocutor->id, Yii::$app->user->id);
                                    $countMessage = Message::countMessage($interlocutor->id, Yii::$app->user->id);
                                    ?>

                                    <?php if($countMessage == 0) :?>
                                    <p class="text-muted text_<?= $interlocutor->id ?>"><?= $lastMessage->text  ?></p>
                                        <small class="text-muted online green status_<?= $interlocutor->id ?>"></small>
                                    <div class="chat-alert alert_<?= $interlocutor->id ?>"><?= $countMessage ?></div>
                                    <?php else :?>
                                    <p class="text-muted text_<?= $interlocutor->id ?>"><?= $lastMessage->text  ?></p>
                                        <small class="text-muted online green status_<?= $interlocutor->id ?>"></small>
                                    <div class="chat-alert alert_<?= $interlocutor->id ?>"><?= $countMessage ?></div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </a>
                    </li>
                    <?php endforeach; ?>

                </ul>
                <?php /*Pjax::end() */?>
            </div>
            <!-- Contact List in Left-->
            <!--Contact List in Left End-->

        </div>
    </div>
</div>
