<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 04.03.2019
 * Time: 19:31
 */
use app\models\Message;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\CastomController;
/*$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';*/
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['interlocutor'], 'id'=>$id];
$this->params['breadcrumbs'][] = $this->title;
$statusAvatar = $userName->getImage();
?>

<div class="chat-room">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs contact-list">
         <!--       <li>

                    <div class="contact">
                        <?php /*if($statusAvatar != null) : */?>
                            <img src="<?/*= $img->getUrl('300x300') */?>" alt="<?/*= $userName->username */?>" class="profile-photo-sm pull-left" />
                        <?php /*else :*/?>
                            <img src="http://placehold.it/300x300" alt="<?/*= $userName->username */?>" class="profile-photo-sm pull-left" />
                        <?php /*endif; */?>
                        <div class="msg-preview">
                            <h6><?/*= $userName->username */?></h6>
                            <p class="text-muted">Онлайн</p>
                            <small class="text-muted">Онлайн</small>
                        </div>
                    </div>
                </li>-->
                <li>
                    <a href="#contact-4" data-toggle="tab">
                        <div class="contact">
                            <?php if($statusAvatar != null) : ?>
                            <img src="<?= $statusAvatar->getUrl('300x300') ?>" alt="<?= $userName->username ?>" class="profile-photo-sm pull-left" />
                            <?php else :?>
                            <img src="http://placehold.it/300x300" alt="<?= $userName->username ?>" class="profile-photo-sm pull-left" />
                            <?php endif; ?>
                            <div class="msg-preview">
                                <h6><?= $userName->username ?></h6>
                                <br>
                                <p class="text-muted"></p>
                                <small class="text-muted online" data-id="<?= $id ?>"></small>
                               <!-- <div class="chat-alert">1</div>-->
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="chat-room">
    <div  class="row">


        <div class="col-md-12">
            <!--Chat Messages in Right-->
            <div class="tab-content scrollbar-wrapper wrapper scrollbar-outer mes">
                <div class="tab-pane active" id="contact-1">
                    <div class="chat-body">
                        <ul class="chat-message" data-id = <?= $id ?>>

                            <?php foreach ($messagesQuery as $message) :?>

                                <?php //\app\controllers\CastomController::printr($message);?>
                            <?php $gal = $message->getImages();?>

                                <?php $img = $message->user->getImage() ; if($message->from == $id) :?>
                                    <li class="left mess_<?= $message->id ?>">
                                        <?php if($img != null) :?>
                                <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-left" />
                                <?php else :?>
                                            <img src="http://placehold.it/300x300" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-left" />
                                <?php endif; ?>
                                <div class="chat-item">
                                    <div class="chat-item-header">
                                        <h5><?= $message->user->username ?></h5>
                                        <small class="text-muted">
                                            <?= $message->creatdate ?>
                                        </small>
                                    </div>
                                    <p><?= $message->text ?></p>
                                    <?php if ($gal != null) : ?>


                                            <?php  foreach ($gal as $file) :?>



                                                    <a data-fancybox="gallery" class="gal img-responsive" href="<?= Url::to($file->getUrl())?>">
                                                        <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl('100x100'))?>" alt="">
                                                    </a>


                                            <?php endforeach; ?>




                                    <?php endif; ?>
                                </div>
                            </li>
                                <?php $img = $message->user->getImage() ; elseif($message->from == Yii::$app->user->id) : ?>
                                    <li class="right  mess_<?= $message->id ?>">
                                        <?php if($img != null) :?>
                                            <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-right" />
                                        <?php else :?>
                                            <img src="http://placehold.it/300x300" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-right" />
                                        <?php endif; ?>
                                <div class="chat-item m_<?=$message->id?>">
                                    <div class="chat-item-header">
                                        <h5>
                                            <?= $message->user->username ?>

                                        </h5>
                                        <small class="text-muted">
                                            <a href="#" class="btn text-primary edit-mes" data-id="<?= $message->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a href="#" class="btn text-danger del-mes" data-id="<?= $message->id ?>"><i class="fa fa-trash" aria-hidden="true" ></i></a>

                                            <?= $message->creatdate ?>
                                        </small>
                                    </div>
                                    <p><?= $message->text ?></p>
                                    <?php if ($gal != null) : ?>

                                        <?php foreach ($gal as $file) :?>


                                                <a data-fancybox="gallery" class="gal img-responsive" href="<?= Url::to($file->getUrl())?>">
                                                    <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl('100x100'))?>" alt="">
                                                </a>



                                        <?php endforeach; ?>




                                    <?php endif; ?>
                                </div>
                            </li>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div><!--Chat Messages in Right End-->

            <div class="send-message">

                <div class="create-post">
                    <div class="row">
                        <?php $form = ActiveForm::begin([
                            'fieldConfig' => [
                                'options' => [

                                    'tag' => false,

                                ],

                            ],
                            'options' => [
                                'class' => 'addMes',
                                'enctype' => 'multipart/form-data'
                            ]

                        ])?>
                        <div class="col-md-8 col-sm-8">

                            <div class="form-group message">


                                <?= $form->field($model, 'text', ['template'=> "{input}"])->textarea(['autofocus' => true,  /*'wrap' => "hard",*/ 'cols'=>"20", 'rows'=>"1", 'class' => 'form-control emodje'])->label(false) ?>
                                <?= $form->field($model, 'to')->hiddenInput(['value' => $id])->label(false) ?>
                                <?= $form->field($model, 'id')->hiddenInput(['value' => '0'])->label(false) ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="tools">
                                <ul class="publishing-tools list-inline">
                                    <li class="hidden-xs hidden-sm"><a href="#" class="emo-mes"><i class="fa fa-smile-o" aria-hidden="true"></i></a></li>
                                    <li>
                                        <?= $form->field($model, 'images[]', ['template'=> "{input}"])->fileInput(['multiple' => true, 'accept' => 'image/*', 'class' => 'image'])->label(false) ?>
                                        <label for="message-images" class="file"><i class="ion-images"></i></label></li>
                                    <!--<li><a href="#"><i class="ion-ios-videocam"></i></a></li>-->
                                    <li><a href="#" class="tooltips"><i class="fa fa-question" aria-hidden="true"></i><span><p class="tool">Отправить: Enter</p><p class="tool">Перенос строки: Ctrl+Enter</p></span></a></li>
                                </ul>
                                <?= yii\helpers\Html::submitButton('<i class="fa fa-comment-o" aria-hidden="true"></i>', ['class' => 'btn btn-primary pull-right']) ?>
                                <!--<button class="btn btn-primary pull-right">Publish</button>-->
                            </div>
                        </div>

                        <?php ActiveForm::end() ?>
                        <div id="image_preview" class="row">




                        </div>
                        <div id="url_preview"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


<?= $this->render('js'); ?>



