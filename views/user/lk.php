<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 14.03.2019
 * Time: 22:15
 */
use yii\helpers\Url;
$img = $model->getImage();
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['interlocutor']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="timeline-cover">

    <!--Timeline Menu for Large Screens-->
    <div class="timeline-nav-bar hidden-sm hidden-xs">
        <div class="row">
            <div class="col-md-5">
                <div class="profile-info">
                    <?php if($img != null) :?>
                        <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $model->username ?>" class="img-responsive profile-photo" />
                    <?php else :?>
                        <img src="http://placehold.it/300x300" alt="<?= $model->username ?>" class="img-responsive profile-photo" />
                    <?php endif; ?>
                    <h3><?= $model->username ?></h3>
                   <!-- <p class="text-muted">Creative Director</p>-->
                </div>
            </div>
            <div class="col-md-7">
                <!--<ul class="list-inline profile-menu">
                    <li><a href="timeline.html">Timeline</a></li>
                    <li><a href="timeline-about.html" class="active">About</a></li>
                    <li><a href="timeline-album.html">Album</a></li>
                    <li><a href="timeline-friends.html">Friends</a></li>
                </ul>-->
                <ul class=" list-inline  profile-menu">
                    <?php if($model->id != Yii::$app->user->id) :?>
                    <li><a href="<?= Url::to(['/user/message', 'id' => $model->id])?>" class="btn-warning"><i class="icon ion-chatboxes"></i> Написать</a></li>
                    <!--<li><a href="#" class="btn-success"><i class="icon ion-ios-people-outline"></i> В друзья</a></li>-->
                    <?php endif; ?>
                    <?php if($model->id == Yii::$app->user->id) :?>
                    <li><a href="<?= Url::to('/user/option') ?>" class="btn-danger"><i class="fa fa-cog"></i> Настройки</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div><!--Timeline Menu for Large Screens End-->

    <!--Timeline Menu for Small Screens-->
    <div class="navbar-mobile hidden-lg hidden-md">
        <div class="profile-info">
            <?php if($img != null) :?>
                <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $model->username ?>" class="img-responsive profile-photo" />
            <?php else :?>
                <img src="http://placehold.it/300x300" alt="<?= $model->username ?>" class="img-responsive profile-photo" />
            <?php endif; ?>
            <h4><?= $model->username ?></h4>
           <!-- <p class="text-muted">Creative Director</p>-->
        </div>
        <div class="mobile-menu">

            <?php if($model->id != Yii::$app->user->id) :?>
                <a href="<?= Url::to(['/user/message', 'id' => $model->id])?>" class="btn-warning"><i class="icon ion-chatboxes"></i> Написать</a>
                <!--<a href="#" class="btn-success"><i class="icon ion-ios-people-outline"></i> В друзья</a>-->
            <?php endif; ?>
            <?php if($model->id == Yii::$app->user->id) :?>
                <a href="<?= Url::to('/user/option') ?>" class="btn-danger"><i class="fa fa-cog"></i> Настройки</a>
            <?php endif; ?>
        </div>
    </div><!--Timeline Menu for Small Screens End-->

</div>
<div class="clearfix"></div>


