<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 11.03.2019
 * Time: 16:35
 */

use yii\helpers\Html;
use yii\helpers\Url;
$img = $model->user->getImage();

?>
    <?php $gal = $model->getImages()?>
<li class="right  mess_<?= $model->id ?>">
    <?php if($img != null) : ?>
        <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $model->user->username ?>" class="profile-photo-sm pull-right" />
    <?php else :?>
        <img src="http://placehold.it/300x300" alt="<?= $model->user->username ?>" class="profile-photo-sm pull-right" />
    <?php endif; ?>
    <div class="chat-item m_<?=$model->id?>">
        <div class="chat-item-header">
            <h5>
                <?= $model->user->username ?>
            </h5>
            <small class="text-muted">
                <a href="#" class="btn text-primary edit-mes" data-id="<?= $model->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="#" class="btn text-danger del-mes" data-id="<?= $model->id ?>"><i class="fa fa-trash" aria-hidden="true" ></i></a>
                <?= $model->creatdate ?>
            </small>
        </div>
        <p><?= $model->text ?></p>
        <?php if ($gal != null) : ?>
                <?php  foreach ($gal as $file) :?>
                    <a data-fancybox="gallery" class="gal img-responsive" href="<?= Url::to($file->getUrl())?>">
                        <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl('100x100'))?>" alt="">
                    </a>
                <?php endforeach; ?>
        <?php endif; ?>
    </div>
</li>
