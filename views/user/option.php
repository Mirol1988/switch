<?php
/**
 * Created by PhpStorm.
 * User: Mirol
 * Date: 14.03.2019
 * Time: 23:20
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="edit-profile-container">
            <div class="block-title">
                <h4 class="grey"><i class="icon ion-android-checkmark-circle"></i><?=$this->title ?></h4>
                <div class="line"></div>
                <!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate</p>-->
                <!--<div class="line"></div>-->
            </div>
            <div class="edit-block">


                <?php $form = ActiveForm::begin(); ?>

               <!-- <div class="row">-->
                    <div class="form-group col-xs-6">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group col-xs-6">
                        <?= $form->field($model, 'email')->textInput() ?>
                    </div>
               <!-- </div>-->

               <!-- <div class="row">-->
                    <div class="form-group col-xs-12">
                        <?= $form->field($model, 'image')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

                        <?php $gal = $model->getImage();//var_dump($gal)?>

                        <?php if (!$model->isNewRecord && $gal != null) : ?>
                            <div class="row">


                                <div class="col-sm-4 col-md-2 col-lg-2 del_<?=$gal->id?>">
                                    <a href="#" class="thumbnail">
                                        <?= Html::img("{$gal->getUrl('150x150')}")?>
                                        <span><i class="fa fa-times-circle del fa-lg delFoto" data-id="<?= $gal->id ?>" data-img = "<?= $gal->id ?>"></i></span>
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                <!--</div>-->


                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>

            </div>

</div>


