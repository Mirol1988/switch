<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 11.03.2019
 * Time: 17:04
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php foreach ($messages as $message) :?>
    <?php $gal = $message->getImages(); $img = $message->user->getImage();?>
    <li class="left  mess_<?= $message->id ?>">
        <?php if($img != null) : ?>
        <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-left" />
        <?php else :?>
        <img src="http://placehold.it/300x300" alt="<?= $message->user->username ?>" class="profile-photo-sm pull-left" />
        <?php endif; ?>
        <!--<img src="<?/*= $message->user->getImages()->getUrl('300x300') */?>" alt="<?/*= $message->user->username */?>" class="profile-photo-sm pull-left" />-->
        <div class="chat-item">
            <div class="chat-item-header">
                <h5><?= $message->user->username ?></h5>
                <small class="text-muted">
                    <?= $message->creatdate ?>
                </small>
            </div>
            <p><?= $message->text ?></p>
            <?php if ($gal != null) : ?>
                    <?php  foreach ($gal as $file) :?>
                        <a data-fancybox="gallery" class="gal img-responsive" href="<?= Url::to($file->getUrl())?>">
                            <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl('100x100'))?>" alt="">
                        </a>
                    <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </li>
<?php endforeach;?>