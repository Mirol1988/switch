<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 25.02.2019
 * Time: 13:04
 */

use yii\helpers\Url;
use app\controllers\CastomController;
use app\models\Comment;


?>

<div class="post-content">

    <!--Post Date-->
    <div class="post-date">

    </div><!--Post Date End-->
    <?php $gal = $model->getImages();//var_dump($gal)
        $count = count($gal)
    ?>

    <?php if($count > 1) :?>
    <div class="grid">
        <div class="grid-sizer"></div>
        <?php  foreach ($gal as $file) :?>

            <div class="grid-item">
                <a data-fancybox="gallery" class="gal" href="<?= Url::to($file->getUrl())?>">
                    <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl())?>" alt="">
                </a>
            </div>
        <?php endforeach; ?>
    </div>

    <?php elseif($count == 1) :?>
        <?php  foreach ($gal as $file) :?>
            <a data-fancybox="gallery" class="gal" href="<?= Url::to($file->getUrl())?>">
                <img class="img-responsive img-gallary post-image" src="<?= Url::to($file->getUrl())?>" alt="">
            </a>
        <?php endforeach; ?>
    <?php endif;?>


    <!--<img src="http://placehold.it/1920x1280" alt="post-image" class="img-responsive post-image" />-->
    <!---->
    <div class="post-container foc_<?= $model->id ?>">


        <!--<img src="http://placehold.it/300x300" alt="user" class="profile-photo-md pull-left" />-->
        <?php $img = $model->author->getImage();?>
        <?php if($img != null) :?>
            <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $model->author->username ?>" class="profile-photo-md pull-left" />
        <?php else :?>
            <img src="http://placehold.it/300x300" alt="<?= $model->author->username ?>" class="profile-photo-md pull-left" />
        <?php endif; ?>
        <div class="post-detail">
            <div class="user-info">
                <h5>
                    <a href="<?= Url::to(['/user/lk', 'id' => $model->author->id]) ?>" class="profile-link">
                        <?php $role = CastomController::currentUserRoleIs($model->author->id); ?>
                        <?php if($role == 'moder' || $role == 'redactor' || $role == 'admin') :?>
                            Custom Nintendo
                        <?php else :?>
                            <?=$model->author->username ?>
                        <?php endif;?>


                    </a>
                </h5>

                <p class="text-muted">Опубликованно: <?= $model->date ?></p>
            </div>
            <div class="reaction">

                <a class="btn text-green jslike"  data-id="<?= $model->id ?>"><i class="icon ion-thumbsup"></i> <?= $model->like ?></a>
                <a class="btn text-red jsdislike" data-id="<?= $model->id ?>"><i class="fa fa-thumbs-down"></i> <?= $model->dislike ?></a>
                <?php if(Yii::$app->user->can('redactor') ||
                    $model->id_user == Yii::$app->user->id) :?>

                    <a href="<?= Url::to(['/post/update', 'id' => $model->id]) ?>" class="btn text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a href="<?= Url::to(['/post/delete', 'id' => $model->id]) ?>" class="btn text-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                <?php endif; ?>
            </div>
            <div class="line-divider"></div>
            <div class="post-text text_<?=$model->id?>" >



                <?php /*if(CastomController::countStr($model->text) > 1050):*/?><!--
                <p class="text_<?/*=$model->id*/?>"><?/*= CastomController::heh(CastomController::substr_close_tags($model->text, 20), $model->id_user)*/?></p>

                --><?php /*else :*/?>
                    <p><?= CastomController::heh($model->text, $model->id_user)?></p>
               <!-- --><?php /*endif; */?>
                <?php if($model->magnet != ''){
                    echo '<p><a href="'.$model->magnet .'">magnet ссылка</a></p>';
                } ?>

            </div>






            <div class="line-divider"></div>
            <div class="c_<?= $model->id?>">
            <?php $moments = $model->comment?>
            <?php foreach ($moments as $comment) :?>
                <div class="post-comment post-comment_<?=$comment->id ?>">
                    <?php $img =$comment->author->getImage();?>
                    <?php if(isset($img)) :?>
                        <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $comment->author->username ?>" class="profile-photo-sm" />
                    <?php else :?>
                        <img src="http://placehold.it/300x300" alt="<?= $comment->author->username ?>" class="profile-photo-sm" />
                    <?php endif; ?>
                    <p><a href="<?= Url::to(['/user/lk', 'id' => $comment->id_user]) ?>" class="profile-link "><?= $comment->author->username ?></a>
                    <p class="com_<?=$comment->id ?>">&nbsp; &nbsp;
                        <?=CastomController::namea($comment->text, $comment->id_user) ?>
                    </p>
                    <!--</p>-->
                    <a href="#" class="btn text-primary com-com"  data-com="<?= $model->id?>" data-username="<?= $comment->author->username ?>" data-user="<?= $comment->id_user ?>"><i class="fa fa-comments" aria-hidden="true"></i></a>
                    <?php if($comment->id_user == Yii::$app->user->id || Yii::$app->user->can('redactor')) :?>
                        <a href="#" class="btn text-primary edit t-e_<?=$comment->id?>" data-id="<?=$comment->id?>"  data-post="<?=$model->id?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="#" class="btn text-danger delitecom del-com_<?=$comment->id?>" data-id="<?=$comment->id?>"><i class="fa fa-trash" aria-hidden="true" ></i></a>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

            </div>

            <div class="pull-right">
                <?php /*if(CastomController::countStr($model->text) > 300):*/?><!--
                <p><a href="#" class="all-text" data-id="<?/*=$model->id*/?>">Показать полностью</a></p>
                --><?php /*endif; */?>
                <?php $comCount = count($model->allcomments); if($comCount >= 2 ) : ?>
                <p><a href="#" class="all-comments" data-id="<?=$model->id?>">Все комментарии (<?= $comCount ?>)</a></p>
                <?php endif; ?>
            </div>
            <div class="post-comment">
                <?php /*$avatar = CastomController::getUserImg(); if($avatar != null) :*/?><!--
                    <img src="<?/*= $avatar */?>" alt="<?/*= $model->author->username */?>" class="profile-photo-sm" />
                <?php /*else :*/?>
                    <img src="http://placehold.it/300x300" alt="<?/*= $model->author->username */?>" class="profile-photo-sm" />
                --><?php /*endif; */?>
                   <!-- <img src="http://placehold.it/300x300" alt="" class="profile-photo-sm" />-->
                    <textarea name="text" class="form-control t_<?=$model->id?> emodje_<?= $model->id?> test"  placeholder="Комментарий" cols="30" rows="10" wrap="hard"></textarea>
                    <input type="hidden" name="id_user" class="form-control u_<?=$model->id?>" placeholder="Комментарий"  value="<?= Yii::$app->user->id ?>">
                    <input type="hidden" name="id_user_otv" class="form-control u_o_<?=$model->id?>"  placeholder="Комментарий" value="0" >
                    <input type="hidden" name="id" class="form-control id_com_<?=$model->id?>"  placeholder="Комментарий" value="" >
                    <input type="hidden" name="date" class="form-control date_<?=$model->id?>"  placeholder="Комментарий" value="" >
                    <button class="btn btn-link emo hidden-xs hidden-sm" data-id="<?=$model->id?>">
                        <i class="fa fa-smile-o" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-link com-tol btn_<?=$model->id?>" data-post="<?=$model->id?>">
                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                    </button>
            </div>
        </div>
    </div>
</div>







