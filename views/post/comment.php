<?php
/**
 * Created by PhpStorm.
 * User: iimikhalev
 * Date: 27.02.2019
 * Time: 13:34
 */

use yii\helpers\Url;
use app\controllers\CastomController;
?>

<div class="post-comment post-comment_<?=$model->id ?>"">
    <?php $img = $model->author->getImage();?>
    <?php if(isset($img)) :?>
        <img src="<?= $img->getUrl('300x300') ?>" alt="<?= $model->author->username ?>" class="profile-photo-sm" />
    <?php else :?>
        <img src="http://placehold.it/300x300" alt="<?= $model->author->username ?>" class="profile-photo-sm" />
    <?php endif; ?>
    <p><a href="<?= Url::to(['/user/lk', 'id' => $model->author->id]) ?>" class="profile-link"><?= $model->author->username ?> </a>
    <p class="com_<?=$model->id ?>">&nbsp; &nbsp;
         <?=CastomController::namea($model->text, $model->author->id) ?>
    </p>
    <a href="#" class="btn text-primary com-com"  data-com="<?= $model->id_post?>" data-username="<?= $model->author->username ?>"  data-user="<?= $model->author->id ?>"><i class="fa fa-comments" aria-hidden="true"></i></a>
    <?php if(Yii::$app->user->can('redactor') ||
        $model->id_user == Yii::$app->user->id) :?>

        <a href="#" class="btn text-primary edit t-e_<?=$model->id?>" data-id="<?=$model->id?>"  data-post="<?=$model->id_post?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
        <a href="#" class="btn text-danger delitecom del-com_<?=$model->id?>" data-id="<?=$model->id?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
    <?php endif; ?>
</div>
