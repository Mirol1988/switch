<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>
<!--<script>
    tinymce.init({
        menubar:false,
        statusbar: false,
    });
</script>-->
<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>





    <?= $form->field($model, 'text')->widget(TinyMce::className(), [
        'options' => ['rows' => 16],
        'language' => 'ru',

        'clientOptions' => [
            'menubar' => false,
            'statusbar' => false,
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste image",
            ],
            'toolbar' => "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

            'image_dimensions' =>  false,
        ]
    ]);?>

    <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?= $form->field($model, 'magnet')->textInput(['maxlength' => true]) ?>


    <?php if(Yii::$app->user->can('redactor')):?>
    <?php $model->cat = 0; ?>
        <?= $form->field($model, 'cat')
            ->radioList(['0' => 'Нет', '1' => 'Да']) ?>
    <?php endif; ?>









    <?php $gal = $model->getImages();//var_dump($gal)?>

    <?php if (!$model->isNewRecord && $model->getImages() != null) : ?>
        <div class="row">


            <?php  foreach ($gal as $file) :?>

                <div class="col-sm-4 col-md-2 col-lg-2 del_<?=$file->id?>">
                    <a href="#" class="thumbnail">
                        <?= Html::img("{$file->getUrl('150x150')}")?>
                        <span><i class="fa fa-times-circle del fa-lg delFoto" data-id="<?= $model->id ?>" data-img = "<?= $file->id ?>"></i></span>
                    </a>
                </div>

            <?php endforeach; ?>
        </div>
    <?php endif; ?>



    <div class="form-group">
        <?= Html::submitButton('Добавить пост', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
