<?php

namespace app\controllers;

use alex290\spreadsheet\Excel;
use app\models\Games;
use app\models\Iso;
use app\models\Post;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use GuzzleHttp\Client;
use yii\web\UploadedFile;
use yii\helpers\Url;
use app\controllers\CastomController;
use app\models\User;
use VK\Client\VKApiClient;/* CallbackApiMyHandler*/
use app\models\CallbackApiMyHandler;
use VK\CallbackApi\LongPoll\VKCallbackApiLongPollExecutor;


class SiteController extends CastomController
{

    public $Password;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        // неавторизованному пользователю доступны только эти действия
                        'actions' => ['login', 'registration', 'vk'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('/post/index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'registr';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('site/index');
        }

        $this->setMeta('Вход');



        $model = new LoginForm();

        //$model->scenario = 'login';

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/post/index');
           /* $user = User::find()->where(['email' => $model->email])->one();
            if(!$user) {
                Yii::$app->session->setFlash('error', 'Пользователя с таким Email не существует.');
                return $this->redirect('site/registration');
            }elseif($user->active == 0){
                Yii::$app->session->setFlash('info', 'Вы не потвердили свой Email.');
                return $this->render('login', [
                    'model' => $model,
                ]);
            }else{
                $model->login();

            }*/
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }


    /**
     * @return string|Response
     * @throws \yii\base\Exception
     *
     * Регистрация пользователя
     */
    public function actionRegistration()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('post/index');
        }

        $this->layout = 'registr';

        $this->setMeta('Регистрация');

        $model = new User();

        $model->scenario = 'registration';

        if ($model->load(Yii::$app->request->post()))
        {
            if (!User::find()->where(['email' => $model->email])->limit(1)->all())
            {
                $this->Password = $model->password;

                $model->password = Yii::$app->security->generatePasswordHash($model->password);
                $model->code = Yii::$app->getSecurity()->generateRandomString(8);


                if($model->save())
                {
                    $model->sendCongirmationLink();
                    /**
                     * Добавление роль пользователя и сохраняем
                     */
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole('user');
                    $auth->assign($authorRole, $model->id);


                    Yii::$app->session->setFlash('success', 'Вам  отправлена ссылка с потверждением вашего Email');
                    return $this->goHome();
                }
                else
                {

                    $model->password = $this->Password;
                    return $this->render('registration', compact('model'));
                }
            }
            else
            {
                Yii::$app->session->setFlash('info', 'Пользователь с таким Email существует, попробуйте востановить пароль.');
                return $this->goHome();
            }
        }

        $model->password = $this->Password;

        return $this->render('registration', compact('model'));
    }


    /**
     * @return Response
     * Потверждение Email
     */
    public function actionConfirmemail()
    {
        $code = Yii::$app->request->get('code');
        $email = Yii::$app->request->get('email');

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = User::find()->where(['code' => $code, 'email' => $email])->one();

        if(!$user)
        {
            Yii::$app->session->setFlash('error', 'Не удалось активировать аккаунт, обратитесь к Администрации сайта.');
            return $this->goHome();
        }

        if ($user->active == 0) {
            $user->code = '';
            $user->active = User::ACTIVE_USER;

            if ($user->save()) {
                //$user->login();
                Yii::$app->session->setFlash('success', 'Аккаунт активирован');
                return $this->redirect('site/login');
            }

        } else {

            Yii::$app->session->setFlash('error', 'Не удалось активировать аккаунт, обратитесь к Администрации сайта.');
            return $this->goHome();
        }
    }





    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionExel(){

        //$file = '/xampp/htdocs/switch/web/XCI.xlsx';
        //$file = '/xampp/htdocs/switch/web/CustomXCI.xlsx';
        //$file = '/xampp/htdocs/switch/web/NSPbase.xlsx';
        //$file = '/xampp/htdocs/switch/web/NSPdlc.xlsx';
        //$file = '/xampp/htdocs/switch/web/NSPupdate.xlsx';
        $file = '/xampp/htdocs/switch/web/AmiiBo.xlsx';

        $data = Excel::import($file);


        foreach ($data as $key => $value){

            $model = new Games();

            $model->title = $value['Name'];
            $model->date = date('Y-m-d H:i:s', strtotime($value['Date']));
            $model->cat = 6;
            $model->url = $value['URL'];
            $model->onedrive1 = $value['onedrive'];
            $model->onedrive2 = $value['onedrivemirror2'];
            $model->f = $value['1F'];
            $model->gmirror1 = $value['gmirror1'];
            $model->gmirror2 = $value['gmirror2'];
            $model->untrimmed = $value['Untrimmed'];
            $model->md5 = $value['MD5'];

            if($model->save()){
                echo '<pre>';
                print_r($value);
                echo '</pre>';
            }else{
                echo '<pre>';
                print_r($model->errors);
                echo '</pre>';
            }

        }
    }

    public function actionPars(){
        //ini_set('max_execution_time', 9999999999999999999999999999999999);
        header('Content-Type: text/html; charset=utf-8', true);
        $client = new Client();
        $url = 'https://pkg.customfw.xyz/games/index?SearchGames%5Bname%5D=&SearchGames%5Breleasetype%5D=SCENE&SearchGames%5Bdate%5D=&SearchGames%5Bcode%5D=&SearchGames%5Bregion%5D=&SearchGames%5Bgenre%5D=&SearchGames%5Blang%5D=&SearchGames%5Bversion%5D=&SearchGames%5Bsize%5D=&vid=table&page=';
        $fin = 17;
        //$fin = 60;
        $urls = array();
        for($i = 1; $i< 3; $i++){
           $urls[] =  $url . $i;


        }
        $i = 0;
        $attr = array();
        foreach ($urls as $url){
            $res = $client->request('GET', $url);
            $body = $res->getBody();
            $document = \phpQuery::newDocumentHTML($body);
            $divs = $document->find('table.table')->find('tr');
            //CastomController::printr($divs);
            foreach ($divs as $div){
                $pq = pq($div);
                //$sel[$i]['a'] = $pq->find('a')->attr('href');
                $attr[$i]['name'] = $pq->find('td')->eq(1)->find('a.tool')->html();
                $attr[$i]['img'] = $pq->find('td')->eq(1)->find('a')->attr('data-original-title');
                $attr[$i]['magnet'] = $pq->find('td')->eq(10)->find('a')->attr('href');
                $i++;
            }

        }

        CastomController::printr($attr);
        exit;

        foreach ($attr as $a){
            $model = new Iso();
            $model->name = $a['name'];
            $model->title = $a['title'];
            /*CastomController::printr($a['name']);*/
            $model->save();
        }


        exit;
    }
    function send()
    {
        $url = 'https://api.vk.com/method/wall.get';
        $params = array(
            'owner_id' => '-178659422',
            //'owner_id' => '-122341224', //switch open
            'count' => 100,
            'filter' => 'all',
            //'domain' => 'https://vk.com/nincfw',
            //'access_token' => 'a8c5f2f5f78c477fd6ee21708c09a97b4b9ebc0b7935d6d4f0b2b318c968df8e6fd20f79e1df22027f1c5',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            'access_token' => '16a2675f16a2675f16a2675fcd16cb1cc2116a216a2675f4a3583fe06b43d046f9ca1f7',  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            //'access_token' =>  md5(api_id + '_' + viewer_id + '_' + api_secret),
            //'access_token' =>  md5('6912925' + '_' + '167914520' + '_' + 'O7B29s9BGShmOdp6ZH7o'),
            'v' => '5.37',
        );

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return $result;
    }
    public function actionVk()
    {



        $this->layout = false;
        $array = json_decode($this->send());


        $posts = array();
        foreach ($array as  $value){
            foreach ($value->items as $item){
                $posts[] = $item ;
            }
        }
        unset($posts[0]);

       

        $postSeve = array(); // масив для разбора поста
        $i = 0;
        foreach ($posts as $post){
            if($post->from_id == '-178659422'){
                $postSeve[$i]['id'] = $post->id ;
                $postSeve[$i]['date'] = date('Y-m-d H:i:s', $post->date);

                $postSeve[$i]['text'] = '<p>'.nl2br($post->text).'</p>';
                if($post->attachments){
                    foreach ($post->attachments as $attachments){
                        if($attachments->type == 'link'){
                            $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->link->url.'" >'.$attachments->link->title.'</a></p><p>';
                            //echo $postSeve[$i]['text'];
                            //CastomController::printr($attachments->link->photo->photo_604);
                        }
                        if($attachments->type == 'photo'){
                            $postSeve[$i]['photo'][] = $attachments->photo->photo_604;
                        }
                        if($attachments->type == 'doc'){
                            $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->doc->url.'">'.$attachments->doc->title.'</a></p>';
                        }
                    }

                }
                if($post->copy_history){
                    foreach ($post->copy_history as $copy_history){
                        $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p>'.nl2br($copy_history->text).'</p>';
                        if($copy_history->attachments){
                            foreach ($copy_history->attachments as $attachments){
                                if($attachments->type == 'link'){
                                    $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->link->url.'" >'.$attachments->link->title.'</a></p><p>';
                                    //echo $postSeve[$i]['text'];
                                    //CastomController::printr($attachments->link->photo->photo_604);
                                }
                                if($attachments->type == 'photo'){
                                    $postSeve[$i]['photo'][] = $attachments->photo->photo_604;
                                }
                                if($attachments->type == 'doc'){
                                    $postSeve[$i]['text'] = $postSeve[$i]['text'] . '<p><a href="'.$attachments->doc->url.'">'.$attachments->doc->title.'</a></p>';
                                }
                            }

                        }
                    }
                    //CastomController::printr($post->copy_history);
                    //
                }
                //CastomController::printr($post);
            }
            $i++;
                //$model = Post::find()->where(['id_vk' => $post->id]);

        }

        foreach ($postSeve as $save){
            $item = Post::find()->where(['vk_id' => $save['id']])->one();
            //echo
            //CastomController::printr($save['text']);
            if(!$item){
                $model = new Post();
                $model->text = $save['text'];
                $model->vk_id = $save['id'];
                $model->date = $save['date'];
                $model->id_user = 1;
                if($model->save()){
                    if($save['photo']){
                        foreach ($save['photo'] as $scrin) {
                            $img = $scrin;
                            if ($img) {
                                $nameFile = substr(md5(microtime() . rand(0, 9999)), 0, 20);
                                $localPath = \Yii::getAlias('@webroot') . '/img/tempImg/' . $nameFile . '.jpg';
                                file_put_contents($localPath, file_get_contents($img));
                                $model->attachImage($localPath, false);
                                //unset($localPath);
                            }
                        }
                        CastomController::printr($model->text);
                    }
                }else {
                    CastomController::printr($model->errors);
                }


            }
        }

        //CastomController::printr($postSeve);
        exit;

    }



        //cc6400e7577426322a5fce79c85120712849f86c250bf8406a45403b4f60ae88ac1b3cedc5f66b8ef8b36
//-178659422
        /**
         * https://oauth.vk.com/authorize?client_id=6912925&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=wall&response_type=token&v=5.37
         */




}
