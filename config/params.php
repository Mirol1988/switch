<?php

return [
    'adminEmail' => 'pkg@customfw.xyz',
    'cat' => [
        /*'' => 'Все категории',*/
        '1' => 'XCI',
        '2' => 'Изменённый XCI',
        '3' => 'NSP',
        '4' => 'NSP Дополнения',
        '5' => 'NSP Обновления',
        '6' => 'AmiiBo'
    ],
    'ps4' => [
        '7' => 'Scene',
        '8' => 'Full',
        '9' => 'PsVr',
        '10' => 'Repack',
        '11' => 'PsVita',
        '12' => 'PSP',
        '13' => 'PsOne'
    ],
    'searchGames' => [
        'games/index',
        'games/view',
        'games/update',
        'games/create',
    ],

    'searchPost' => [
        'post/index',
        'post/view',
        'post/update',
        'post/create',
    ],

    'gamesCat' => [
        '1' => 'XCI',
        '2' => 'Изменённый XCI',
        '3' => 'NSP',
        '4' => 'NSP Дополнения',
        '5' => 'NSP Обновления',
        '6' => 'AmiiBo'
    ]
];
